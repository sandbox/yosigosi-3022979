<?php

/**
 * @file
 * Contains tmx_def_tileset.page.inc.
 *
 * Page callback for TMX Tileset Definition entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for TMX Tileset Definition templates.
 *
 * Default template: tmx_def_tileset.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_tmx_def_tileset(array &$variables) {
  // Fetch TmxDefTileset Entity Object.
  $tmx_def_tileset = $variables['elements']['#tmx_def_tileset'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
