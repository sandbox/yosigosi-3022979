<?php

/**
 * @file
 * Contains tmx_def_template.page.inc.
 *
 * Page callback for TMX Template Definition entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for TMX Template Definition templates.
 *
 * Default template: tmx_def_template.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_tmx_def_template(array &$variables) {
  // Fetch TmxDefTemplate Entity Object.
  $tmx_def_template = $variables['elements']['#tmx_def_template'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
