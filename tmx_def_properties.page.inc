<?php

/**
 * @file
 * Contains tmx_def_properties.page.inc.
 *
 * Page callback for TMX Properties Definition entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for TMX Properties Definition templates.
 *
 * Default template: tmx_def_properties.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_tmx_def_properties(array &$variables) {
  // Fetch TmxDefProperties Entity Object.
  $tmx_def_properties = $variables['elements']['#tmx_def_properties'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
