<?php

/**
 * @file
 * Contains tmx_def_map.page.inc.
 *
 * Page callback for TMX Map Definition entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for TMX Map Definition templates.
 *
 * Default template: tmx_def_map.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_tmx_def_map(array &$variables) {
  // Fetch TmxDefMap Entity Object.
  $tmx_def_map = $variables['elements']['#tmx_def_map'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
