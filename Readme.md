Integra la librería: php-tmx-viewer
https://github.com/sebbu2/php-tmx-viewer


Permissions: 
- administer tmx: Administer TMX.
- view tmx maps:  View published TMX Maps.
- view tmx maps unpublished: View unpublished TMX Maps.
- view tmx definitions: View published TMX Definitions.
- view tmx definitions unpublished: View unpublished TMX Definitions.
- add tmx maps: Create new TMX Maps.
- delete tmx maps: Delete TMX Maps.
- delete own tmx maps: Delete own TMX Maps.
- edit tmx maps: Edit TMX Maps.
- edit tmx definitions: Edit TMX Definitions.

Entidades
- tmx_maps
- tmx_def_maps
- tmx_def_layers
- tmx_def_chunks
- tmx_def_objects
- tmx_def_coordinates
- tmx_def_templates
- tmx_def_tilesets
- tmx_def_tileitems
- tmx_def_frames
- tmx_def_terrains
- tmx_def_wang_colors
- tmx_def_wang_sets
- tmx_def_wang_tiles
- tmx_def_properties



Routes:
/admin/content/tmx/maps
/admin/content/tmx/def_maps
/admin/content/tmx/def_layers
/admin/content/tmx/def_chunks
/admin/content/tmx/def_objects
/admin/content/tmx/def_coordinates
/admin/content/tmx/def_templates
/admin/content/tmx/def_tilesets
/admin/content/tmx/def_tileitems
/admin/content/tmx/def_frames
/admin/content/tmx/def_terrains
/admin/content/tmx/def_wang_colors
/admin/content/tmx/def_wang_sets
/admin/content/tmx/def_wang_tiles
/admin/content/tmx/def_properties

/admin/content/tmx/tmx_map/{tmx_map}
/admin/content/tmx/tmx_map/add
/admin/content/tmx/tmx_map/{tmx_map}/edit
/admin/content/tmx/tmx_map/{tmx_map}/delete

/admin/content/tmx/tmx_def_map/{tmx_def_map}
/admin/content/tmx/tmx_def_map/add
/admin/content/tmx/tmx_def_map/{tmx_def_map}/edit
/admin/content/tmx/tmx_def_map/{tmx_def_map}/delete

/admin/content/tmx/tmx_def_chunk/{tmx_def_chunk}
/admin/content/tmx/tmx_def_chunk/add
/admin/content/tmx/tmx_def_chunk/{tmx_def_chunk}/edit
/admin/content/tmx/tmx_def_chunk/{tmx_def_chunk}/delete
 
/admin/content/tmx/tmx_def_layer/{tmx_def_layer}
/admin/content/tmx/tmx_def_layer/add
/admin/content/tmx/tmx_def_layer/{tmx_def_layer}/edit
/admin/content/tmx/tmx_def_layer/{tmx_def_layer}/delete

/admin/content/tmx/tmx_def_object/{tmx_def_object}
/admin/content/tmx/tmx_def_object/add
/admin/content/tmx/tmx_def_object/{tmx_def_object}/edit
/admin/content/tmx/tmx_def_object/{tmx_def_object}/delete

/admin/content/tmx/tmx_def_template/{tmx_def_template}
/admin/content/tmx/tmx_def_template/add
/admin/content/tmx/tmx_def_template/{tmx_def_template}/edit
/admin/content/tmx/tmx_def_template/{tmx_def_template}/delete

/admin/content/tmx/tmx_def_tileset/{tmx_def_tileset}
/admin/content/tmx/tmx_def_tileset/add
/admin/content/tmx/tmx_def_tileset/{tmx_def_tileset}/edit
/admin/content/tmx/tmx_def_tileset/{tmx_def_tileset}/delete

/admin/content/tmx/tmx_def_tileitem/{tmx_def_tileitem}
/admin/content/tmx/tmx_def_tileitem/add
/admin/content/tmx/tmx_def_tileitem/{tmx_def_tileitem}/edit
/admin/content/tmx/tmx_def_tileitem/{tmx_def_tileitem}/delete

/admin/content/tmx/tmx_def_frame/{tmx_def_frame}
/admin/content/tmx/tmx_def_frame/add
/admin/content/tmx/tmx_def_frame/{tmx_def_frame}/edit
/admin/content/tmx/tmx_def_frame/{tmx_def_frame}/delete

/admin/content/tmx/tmx_def_terrain/{tmx_def_terrain}
/admin/content/tmx/tmx_def_terrain/add
/admin/content/tmx/tmx_def_terrain/{tmx_def_terrain}/edit
/admin/content/tmx/tmx_def_terrain/{tmx_def_terrain}/delete

/admin/content/tmx/tmx_def_wang_color/{tmx_def_wang_color}
/admin/content/tmx/tmx_def_wang_color/add
/admin/content/tmx/tmx_def_wang_color/{tmx_def_wang_color}/edit
/admin/content/tmx/tmx_def_wang_color/{tmx_def_wang_color}/delete

/admin/content/tmx/tmx_def_wang_set/{tmx_def_wang_set}
/admin/content/tmx/tmx_def_wang_set/add
/admin/content/tmx/tmx_def_wang_set/{tmx_def_wang_set}/edit
/admin/content/tmx/tmx_def_wang_set/{tmx_def_wang_set}/delete

/admin/content/tmx/tmx_def_wang_tile/{tmx_def_wang_tile}
/admin/content/tmx/tmx_def_wang_tile/add
/admin/content/tmx/tmx_def_wang_tile/{tmx_def_wang_tile}/edit
/admin/content/tmx/tmx_def_wang_tile/{tmx_def_wang_tile}/delete

/admin/content/tmx/tmx_def_properties/{tmx_def_properties}
/admin/content/tmx/tmx_def_properties/add
/admin/content/tmx/tmx_def_properties/{tmx_def_properties}/edit
/admin/content/tmx/tmx_def_properties/{tmx_def_properties}/delete

/admin/content/tmx/tmx_def_coordinates/{tmx_def_coordinates}
/admin/content/tmx/tmx_def_coordinates/add
/admin/content/tmx/tmx_def_coordinates/{tmx_def_coordinates}/edit
/admin/content/tmx/tmx_def_coordinates/{tmx_def_coordinates}/delete


/admin/structure/tmx

/admin/structure/tmx/tmx_map/settings
/admin/structure/tmx/tmx_map/settings/fields
/admin/structure/tmx/tmx_map/settings/form-display
/admin/structure/tmx/tmx_map/settings/display

/admin/structure/tmx/tmx_def_map/settings
/admin/structure/tmx/tmx_def_map/settings/fields
/admin/structure/tmx/tmx_def_map/settings/form-display
/admin/structure/tmx/tmx_def_map/settings/display

/admin/structure/tmx/tmx_def_chunk/settings
/admin/structure/tmx/tmx_def_chunk/settings/fields
/admin/structure/tmx/tmx_def_chunk/settings/form-display
/admin/structure/tmx/tmx_def_chunk/settings/display

/admin/structure/tmx/tmx_def_layer/settings
/admin/structure/tmx/tmx_def_layer/settings/fields
/admin/structure/tmx/tmx_def_layer/settings/form-display
/admin/structure/tmx/tmx_def_layer/settings/display

/admin/structure/tmx/tmx_def_object/settings
/admin/structure/tmx/tmx_def_object/settings/fields
/admin/structure/tmx/tmx_def_object/settings/form-display
/admin/structure/tmx/tmx_def_object/settings/display

/admin/structure/tmx/tmx_def_template/settings
/admin/structure/tmx/tmx_def_template/settings/fields
/admin/structure/tmx/tmx_def_template/settings/form-display
/admin/structure/tmx/tmx_def_template/settings/display

/admin/structure/tmx/tmx_def_terrain/settings
/admin/structure/tmx/tmx_def_terrain/settings/fields
/admin/structure/tmx/tmx_def_terrain/settings/form-display
/admin/structure/tmx/tmx_def_terrain/settings/display

/admin/structure/tmx/tmx_def_tileset/settings
/admin/structure/tmx/tmx_def_tileset/settings/fields
/admin/structure/tmx/tmx_def_tileset/settings/form-display
/admin/structure/tmx/tmx_def_tileset/settings/display

/admin/structure/tmx/tmx_def_wang_color/settings
/admin/structure/tmx/tmx_def_wang_color/settings/fields
/admin/structure/tmx/tmx_def_wang_color/settings/form-display
/admin/structure/tmx/tmx_def_wang_color/settings/display

/admin/structure/tmx/tmx_def_wang_set/settings
/admin/structure/tmx/tmx_def_wang_set/settings/fields
/admin/structure/tmx/tmx_def_wang_set/settings/form-display
/admin/structure/tmx/tmx_def_wang_set/settings/display

/admin/structure/tmx/tmx_def_wang_tile/settings
/admin/structure/tmx/tmx_def_wang_tile/settings/fields
/admin/structure/tmx/tmx_def_wang_tile/settings/form-display
/admin/structure/tmx/tmx_def_wang_tile/settings/display

/admin/config/tmx

/admin/reports/tmx


Data Definitions

Map

 Type     | Field | Description 
 -------- | ----- | -----------
 int      | tmx_mid |  TMX Map Id
 computed | properties | Array of properties objects (name, value, type).
 computed | tilesets  |  Array of  Tilesets ids
 computed | layers  |  Array of Layers ids
 string   | type | map (since 1.0)
 string   | orientation  |  orthogonal, isometric, staggered or hexagonal
 number   | version  |  The JSON format version
 string   | tiledversion  |  The Tiled version used to save the file
 bool     | infinite  |  Whether the map has infinite dimensions
 int      | height  |  Number of tile rows
 int      | width  |  Number of tile columns
 int      | tileheight  |  Map grid height
 int      | tilewidth  |  Map grid width
 int      | hexsidelength  |  Length of the side of a hex tile in pixels
 string   | backgroundcolor  |  Hex-formatted color (#RRGGBB or #AARRGGBB) (optional)
 string   | renderorder  |  Rendering direction (orthogonal maps only)
 string   | staggeraxis  |  x or y (staggered / hexagonal maps only)
 string   | staggerindex  |  odd or even (staggered / hexagonal maps only)
 int      | nextlayerid  |                   Auto-increments for each layer
 int      | nextobjectid  |  Auto-increments for each placed object

Layer

 Type      | Field | Description
 --------- | ----- | -----------
 int       | tmx_lid | TMX Map Id
 string    | name  |  Name assigned to this layer 
 ref       | tmx_def_map | Referenced Map Definition
 ref       | tmx_def_layer | Referended Layer (optional).
 computed  | properties | Array of properties objects (name, value, type).
 computed  | layers | Array of layers ids. group only
 computed  | chunks | Array of chunks (optional). tilelayer only.
 computed  | objects  |  Array of objects. objectgroup only.
 int       | lid  |  Incremental id - unique across all layers

 string    | type  |  tilelayer, objectgroup, imagelayer or group
 bool      | visible  |  Whether layer is shown or hidden in editor
 double    | opacity  |  Value between 0 and 1
 int       | height  |  Row count. Same as map height for fixed-size maps.
 int       | width  |  Column count. Same as map width for fixed-size maps.
 double    | offsetx  |  Horizontal layer offset in pixels (default: 0)
 double    | offsety  |  Vertical layer offset in pixels (default: 0)
 int       | x  |  Horizontal layer offset in tiles. Always 0.
 int       | y  |  Vertical layer offset in tiles. Always 0.
 string    | image  |  Image used by this layer. imagelayer only.
 ref       | tmx_img_fid |  Image used by this layer. imagelayer only.
 string    | transparentcolor  |  Hex-formatted color (#RRGGBB) (optional, imagelayer only
 text_long | data  | JSON Array of unsigned int (GIDs) or base64-encoded data. tilelayer only.
 string    | compression  |  zlib, gzip or empty (default). tilelayer only.
 string    | encoding  |  csv (default) or base64`. ``tilelayer only.
 string    | draworder  |  topdown (default) or index. objectgroup only.

Chunk

 Type  | Field | Description
 ----- | ----- | -----------
 int   | tmx_cid |  TMX Chunk Id
 ref   | tmx_def_layer | Referended Layer
 text  | data   |  Array of unsigned int (GIDs) or base64-encoded data
 int   | height |  Height in tiles
 int   | width  |  Width in tiles
 int   | x  |  X coordinate in tiles
 int   | y  |  Y coordinate in tiles

Object

 Type     | Field | Description
 -------- | ----- | -----------
 int      | tmx_obid | TMX Object Id
 string   | name  |  String assigned to name field in editor
 computed | polygon  |  A list of x,y coordinates in pixels (array)
 computed | polyline |  A list of x,y coordinates in pixels (array)
 computed | properties |  A list of properties (name, value, type) (array)
 bool     | ellipse  |  Used to mark an object as an ellipse
 int      | gid  |  GID, only if object comes from a Tilemap
 double   | height  |  Height in pixels. Ignored if using a gid.
 int      | id  |  Incremental id - unique across all objects
 bool     | point  |  Used to mark an object as a point
 double   | rotation  |  Angle in degrees clockwise
 string   | template  |  Reference to a template file, in case object is a template instance
 object   | text  |  String key-value pairs
 string   | type  |  String assigned to type field in editor
 bool     | visible  |  Whether object is shown in editor.
 double   | width  |  Width in pixels. Ignored if using a gid.
 double   | x  |  X coordinate in pixels
 double   | y  |  Y coordinate in pixels

Coordinates

 Type   | Field | Description 
 ------ | ----- | -----------
 int    | tmx_crid |  TMX Coordinates Id
 int    | tmx_groupid | Id of a group of coordinates, equals to first coordinate id in the group. 
 int    | crid | Row id
 float | x  | X coordinate in pixels (double)
 float | y  | Y coordinate in pixels (double)

Template (Object Template)
An object template is written to its own file and referenced by any instances of that template.

 Type     | Field | Description
 -------- | ----- | -----------
 int      | tmx_tmid |  TMX Object Template Id
 computed | tileset | External tileset used by the template (optional) 
 ref      | tmx_def_object | The object instantiated by this template (ref) (ref Object, original name: object)
 string   | type  | template file name

Tileset

 Type   | Field | Description
 ------ | ----- | -----------
 int    | tmx_tsid |  TMX Tileset Id
 computed | properties  |  A list of properties (name, value, type) (array).
 computed | terrains  |  Array of Terrains (optional) (array).
 computed | tiles  |  Array of Tiles (optional) (array).
 computed | wangsets  |  Array of Wang sets (since 1.1.5) (array).
 computed | grid | Grid. (optional) Build from fields: gridwidth(width), gridheight(height), gridtype(orientation)
 computed | tileoffset | Tileoffset. (optional) (object) Built from: tileoffsetx(x), tileoffsety(y)
 ref    | tmx_def_map | Referended Map
 ref    | tmx_def_template | Referended Template
 string | name  |  Name given to this tileset
 string | type  |  tileset (for tileset files, since 1.0)
 int    | columns  |  The number of tile columns in the tileset
 int    | margin  |  Buffer between image edge and first tile (pixels)
 int    | spacing  |  Spacing between adjacent tiles in image (pixels)
 int    | tilecount  |  The number of tiles in this tileset
 int    | tileheight  |  Maximum height of tiles in this set
 int    | tilewidth  |  Maximum width of tiles in this set
 int    | firstgid  |  GID corresponding to the first tile in the set
 string | image  |  Image used for tiles in this set
 ref    | tmx_img_fid |  Image used by this layer. imagelayer only. 
 int    | imagewidth  |  Width of source image in pixels
 int    | imageheight  |  Height of source image in pixels
 string | transparentcolor  |  Hex-formatted color (#RRGGBB) (optional)
 string | gridtype  | Grid. Orientation of the grid for the tiles in this tileset (orthogonal or isometric). only isometric (optional)
 int    | gridwidth | Grid. Width of a grid cell (optional)
 int    | gridheight | Grid. Height of a grid cell (optional)
 int    | tileoffsetx | Tileoffset. Horizontal offset in pixels (optional).
 int    | tileoffsety | Tileoffset. Vertical offset in pixels (positive is down) (optional).
 

Tileitem (tile)

 Type     | Field | Description
 -------- | ----- | -----------
 int      | tmx_tlid |  TMX Tile Id
 computed | animation  |  Array of Frames (array)
 computed | properties |  A list of properties (name, value, type) (array)
 computed | terrain    |  Index of terrain for each corner of tile (array)
 ref      | tmx_def_tileset | Referenced Tileset
 ref      | tmx_def_layer |  Layer with type objectgroup (optional) (Original: objectgroup)
 int      | tid  |  Local ID of the tile (Original: id)
 string   | type  |  The type of the tile (optional)
 string   | image  |  Image representing this tile (optional)
 ref      | tmx_img_fid |  Image used by this layer. imagelayer only.
 int      | imageheight  |  Height of the tile image in pixels
 int      | imagewidth  |  Width of the tile image in pixels

Frame

 Type | Field | Description
 ---- | ----- | -----------
 int  | tmx_fid |  TMX Frame Id
 int  | tmx_def_tileitem_group | Referenced Tileitem 
 int  | tmx_def_tileitem_frame |  Tileitem reference representing this frame (Original: tileid)
 int  | duration  |  Frame duration in milliseconds
 

Terrain

 Type   | Field | Description
 ------ | ----- | -----------
 int    | tmx_trid |  TMX Terrain Id
 string | name  |  Name of terrain
 int    | tmx_def_tileset | Referenced Tileset
 int    | tmx_def_tileitem | Referenced Tileitem (Original: tile)


Wang Set

 Type     | Field | Description
 -------- | ----- | -----------
 int      | tmx_wsid |  TMX Wang Set Id
 string   | name |  Name of the Wang set 
 computed | cornercolors | Array of Wang colors (array).
 computed | edgecolors | Array of Wang colors (array).
 computed | wangtiles | Array of Wang tiles (array).
 int      | tmx_def_tileset | Referenced Tileset
 int      | tmx_def_tileitem | Referenced Tileitem (Original: tile)


Wang Tile

 Type     | Field | Description
 -------- | ----- | -----------
 int      | tmx_wtid |  TMX Wang Tile Id
 computed | wangcolors | Wang color indexes (uchar[8]) (array) (Original: wangid)
 int      | tmx_def_wang_set | Referenced Wang Set
 int      | tmx_def_tileitem |  Local ID of tile (Original: tileid)
 bool     | dflip  |  Tile is flipped diagonally
 bool     | hflip  |  Tile is flipped horizontally
 bool     | vflip  |  Tile is flipped vertically


Wang Color

 Type   | Field | Description
 ------ | ----- | -----------
 int    | tmx_wcid |  TMX Wang Color Id
 string | name  |  Name of the Wang color
 int    | tmx_def_wang_set | Referenced Wang Set
 int    | tmx_def_tileitem | Referenced Tileitem (Original: tile)
 int    | tmx_def_wang_tile | Referenced Wang Tile 
 string | group | Group type of Wang colors (corner|edge)   
 string | color |  Hex-formatted color (#RRGGBB or #AARRGGBB)
 float  | probability |  Probability used when randomizing (double)


Properties

 Type   | Field | Description 
 ------ | ----- | -----------
 int   | tmx_pid |  TMX Properties Id
 string | name  | Property name   
 string | tmx_entity_type | Tipo de entidad referenciada. (map, layer, object, tileset, tile) 
 ref    | tmx_entity_id   | Identidad de la entidad referenciada
 string | type  | Property type
 string | value | Property value  
