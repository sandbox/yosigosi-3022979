<?php

/**
 * @file
 * Contains tmx_map.page.inc.
 *
 * Page callback for TMX Map entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for TMX Map templates.
 *
 * Default template: tmx_map.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_tmx_map(array &$variables) {
  // Fetch TmxMap Entity Object.
  $tmx_map = $variables['elements']['#tmx_map'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
