<?php

/**
 * @file
 * Contains tmx_def_coordinates.page.inc.
 *
 * Page callback for TMX Coordinates Definition entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for TMX Coordinates Definition templates.
 *
 * Default template: tmx_def_coordinates.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_tmx_def_coordinates(array &$variables) {
  // Fetch TmxDefCoordinates Entity Object.
  $tmx_def_coordinates = $variables['elements']['#tmx_def_coordinates'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
