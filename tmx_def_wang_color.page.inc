<?php

/**
 * @file
 * Contains tmx_def_wang_color.page.inc.
 *
 * Page callback for TMX Wang Color Definition entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for TMX Wang Color Definition templates.
 *
 * Default template: tmx_def_wang_color.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_tmx_def_wang_color(array &$variables) {
  // Fetch TmxDefWangColor Entity Object.
  $tmx_def_wang_color = $variables['elements']['#tmx_def_wang_color'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
