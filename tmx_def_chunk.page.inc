<?php

/**
 * @file
 * Contains tmx_def_chunk.page.inc.
 *
 * Page callback for TMX Chunk Definition entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for TMX Chunk Definition templates.
 *
 * Default template: tmx_def_chunk.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_tmx_def_chunk(array &$variables) {
  // Fetch TmxDefChunk Entity Object.
  $tmx_def_chunk = $variables['elements']['#tmx_def_chunk'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
