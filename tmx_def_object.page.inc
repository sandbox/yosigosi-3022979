<?php

/**
 * @file
 * Contains tmx_def_object.page.inc.
 *
 * Page callback for TMX Object Definition entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for TMX Object Definition templates.
 *
 * Default template: tmx_def_object.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_tmx_def_object(array &$variables) {
  // Fetch TmxDefObject Entity Object.
  $tmx_def_object = $variables['elements']['#tmx_def_object'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
