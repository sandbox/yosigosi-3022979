<?php

use Symfony\Component\HttpFoundation\Response;
use Drupal\Core\Session\AccountProxyInterface;

/**
 * @file
 * Contains tmx functions.
 */

/**
 * Send a error response with aditional info 
 * 
 * @param integer $status_code
 * @param string $file
 * @param string $point
 * @param string $message
 * @return Response
 */
function tmx__response__error($status_code, $file='', $point='', $message='', $debug_info='') {
  
  $response = new Response();
  $response->setStatusCode($status_code);
  
  $content = [];
  
  $user = \Drupal::currentUser();
  
  $default_message = '';
  switch ($status_code) {
    case 400: $default_message = t('400 Bad Request'); break;
    case 401: $default_message = t('401 Unauthorized'); break;
    case 403: $default_message = t('403 Forbidden'); break;
    case 404: $default_message = t('404 Not Found'); break;
    case 500: $default_message = t('500 Internal Server Error'); break;
    case 502: $default_message = t('502 Bad Gateway'); break;
    case 503: $default_message = t('503 Service Unavailable'); break;
    case 504: $default_message = t('504 Gateway Timeout'); break;
  }
  
  $content[] = t('%text', ['%text' => $default_message] );
  
  if ( $user->isAuthenticated() ) {
    
    if (!empty($message)) {
      $content[] = t('Message: %text', ['%text' => $message] );
    }
    
    if (!empty($point)) {
      $point = preg_replace('`^Drupal\\\`', '', $point);
      $content[] = t('Error point: %text', ['%text' => $point] );
    }
    
    if (!empty($file)) {
      if ( !$user->hasPermission( 'administer site configuration' ) ) {
        $file = preg_replace('`^' . preg_quote(DRUPAL_ROOT . '/' , '`') . '`', '', $file);
      }
      $content[] = t('File: %text', ['%text' => $file] );
    }
    if (!empty($debug_info) && $user->hasPermission( 'administer site configuration' ) ) {
      $content[] = sprintf('<br><br>Debug info:<pre>%s</pre>', $debug_info);      
    }
  }
  
  $response->setContent( implode("\n<br>", $content) );
  
  return $response;
}

function tmx__response__image__png( $gd_resource ) {
  
  $response = NULL;
  $output_buffering_initial = ini_get('output_buffering');
  
  try {
    ini_set( 'output_buffering', 'off' );
    
    // build the image
    ob_get_clean();
    ob_start();
    imagesavealpha( $gd_resource, TRUE );
    imagepng( $gd_resource );
    $content = ob_get_contents();
    ob_end_clean();
    
    // build the response
    $headers = [
      'Content-Type' => 'image/png',
      'Content-Length' => strlen($content),
    ];
    $response = new Response($content, 200, $headers);
  }
  catch ( \Throwable $e) {
    $response = tmx__response__error(503, __FILE__, __FUNCTION__, '', $e->__toString()); 
  }
  
  ini_set( 'output_buffering', $output_buffering_initial );
  
  return $response;
}

