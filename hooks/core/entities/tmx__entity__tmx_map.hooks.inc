<?php

/**
 * @file
 * Contains hooks for tmx_map entity
 */

use Drupal\Core\Access\AccessResult;

/**
 * Implements hook_ENTITY_TYPE_access()
 * Control entity operation access for a specific entity type.
 *
 * @ingroup entity_api
 */
function tmx_tmx_map_access(\Drupal\Core\Entity\EntityInterface $entity, $operation, \Drupal\Core\Session\AccountInterface $account) {
  // No opinion.
  return AccessResult::neutral();
}

/**
 * Implements hook_ENTITY_TYPE_create_access()
 * Control entity create access for a specific entity type.
 *
 * @ingroup entity_api
 */
function tmx_tmx_map_create_access(\Drupal\Core\Session\AccountInterface $account, array $context, $entity_bundle) {
  // No opinion.
  return AccessResult::neutral();
}

/**
 * Implements hook_ENTITY_TYPE_create()
 * Acts when creating a new entity of a specific type.
 */
function tmx_tmx_map_create(\Drupal\Core\Entity\EntityInterface $entity) {
  $a = 1; // TBD
}

/**
 * Implements hook_ENTITY_TYPE_load()
 * Act on entities of a specific type when loaded.
 *
 * @ingroup entity_crud
 */
function tmx_tmx_map_load($entities) {
  foreach ($entities as $entity_id => $entity) {
    $a = 1; // TBD
  }
}

/**
 * Implements hook_ENTITY_TYPE_storage_loadL() 
 * Act on content entities of a given type when loaded from the storage.
 *
 * The results of this hook will be cached if the entity type supports it.
 */
function tmx_tmx_map_storage_load(array $entities) {
  foreach ($entities as $entity_id => $entity) {
    $a = 1; // TBD
  }
}

/**
 * Implements hook_ENTITY_TYPE_presave() 
 * Act on a specific type of entity before it is created or updated.
 *
 * You can get the original entity object from $entity->original when it is an
 * update of the entity.
 *
 * @ingroup entity_crud
 */
function tmx_tmx_map_presave(Drupal\Core\Entity\EntityInterface $entity) {
  $a = 1; // TBD
}

/**
 * Implements hook_ENTITY_TYPE_insert()
 * Respond to creation of a new entity of a particular type.
 *
 * This hook runs once the entity has been stored. Note that hook
 * implementations may not alter the stored entity data.
 *
 * @ingroup entity_crud
 */
function tmx_tmx_map_insert(Drupal\Core\Entity\EntityInterface $entity) {
  $a = 1; // TBD
}

/**
 * Implements hook_ENTITY_TYPE_updatel() 
 * Respond to updates to an entity of a particular type.
 *
 * This hook runs once the entity storage has been updated. Note that hook
 * implementations may not alter the stored entity data. Get the original entity
 * object from $entity->original.
 *
 * @ingroup entity_crud
 */
function tmx_tmx_map_update(Drupal\Core\Entity\EntityInterface $entity) {
  $a = 1; // TBD
}

/**
 * Implements hook_ENTITY_TYPE_predelete() 
 * Act before entity deletion of a particular entity type.
 *
 * @ingroup entity_crud
 */
function tmx_tmx_map_predelete(Drupal\Core\Entity\EntityInterface $entity) {
  $a = 1; // TBD
}

/**
 * Implements hook_ENTITY_TYPE_delete() 
 * Respond to entity deletion of a particular type.
 *
 * This hook runs once the entity has been deleted from the storage.
 *
 * @ingroup entity_crud
 */
function tmx_tmx_map_delete(Drupal\Core\Entity\EntityInterface $entity) {
  $a = 1; // TBD
}

/**
 * Implements hook_ENTITY_TYPE_view() 
 * Act on entities of a particular type being assembled before rendering.
 *
 * @ingroup entity_crud
 */
function tmx_tmx_map_view(array &$build, \Drupal\Core\Entity\EntityInterface $entity, \Drupal\Core\Entity\Display\EntityViewDisplayInterface $display, $view_mode) {
  $a = 1; // TBD
}

/**
 * Implements hook_ENTITY_TYPE_view_alter() 
 * Alter the results of the entity build array for a particular entity type.
 *
 * This hook is called after the content has been assembled in a structured
 * array and may be used for doing processing which requires that the complete
 * entity content structure has been built.
 *
 * If a module wishes to act on the rendered HTML of the entity rather than the
 * structured content array, it may use this hook to add a #post_render
 * callback. Alternatively, it could also implement hook_preprocess_HOOK() for
 * the particular entity type template, if there is one (e.g., node.html.twig).
 *
 * See the @link themeable Default theme implementations topic @endlink and
 * drupal_render() for details.
 *
 * @ingroup entity_crud
 */
function tmx_tmx_map_view_alter(array &$build, Drupal\Core\Entity\EntityInterface $entity, \Drupal\Core\Entity\Display\EntityViewDisplayInterface $display) {
  $a = 1; // TBD
}

/**
 * Implements hook_ENTITY_TYPE_build_defaults_alter() 
 * Alter entity renderable values before cache checking in drupal_render().
 *
 * Invoked for a specific entity type.
 *
 * The values in the #cache key of the renderable array are used to determine if
 * a cache entry exists for the entity's rendered output. Ideally only values
 * that pertain to caching should be altered in this hook.
 *
 * @ingroup entity_crud
 */
function tmx_tmx_map_build_defaults_alter(array &$build, \Drupal\Core\Entity\EntityInterface $entity, $view_mode) {
  $a = 1; // TBD
}

/**
 * Implements hook_ENTITY_TYPE_prepare_form() 
 * Acts on a particular type of entity object about to be in an entity form.
 *
 * This can be typically used to pre-fill entity values or change the form state
 * before the entity form is built. It is invoked just once when first building
 * the entity form. Rebuilds will not trigger a new invocation.
 *
 * @ingroup entity_crud
 */
function tmx_tmx_map_prepare_form(\Drupal\Core\Entity\EntityInterface $entity, $operation, \Drupal\Core\Form\FormStateInterface $form_state) {
  $a = 1; // TBD
}

/**
 * Implements hook_ENTITY_TYPE_field_values_init()
 * Acts when initializing a fieldable entity object.
 *
 * This hook runs after a new entity object or a new entity translation object
 * has just been instantiated. It can be used to set initial values, e.g. to
 * provide defaults.
 *
 * @param \Drupal\Core\Entity\FieldableEntityInterface $entity
 *   The entity object.
 *
 * @ingroup entity_crud
 */
function tmx_tmx_map_field_values_init(\Drupal\Core\Entity\FieldableEntityInterface $entity) {
  $a = 1; // TBD
}


/**
 * Implements hook_ENTITY_TYPE_revision_create()
 * Respond to entity revision creation.
 *
 * @ingroup entity_crud
 */
function tmx_tmx_map_revision_create(Drupal\Core\Entity\EntityInterface $new_revision, Drupal\Core\Entity\EntityInterface $entity, $keep_untranslatable_fields) {
  $a = 1; // TBD
}

/**
 * Implements hook_ENTITY_TYPE_revision_delete()
 * Respond to entity revision deletion of a particular type.
 *
 * This hook runs once the entity revision has been deleted from the storage.
 *
 * @ingroup entity_crud
 */
function tmx_tmx_map_revision_delete(Drupal\Core\Entity\EntityInterface $entity) {
  $a = 1; // TBD
}

/**
 * Implements hook_ENTITY_TYPE_translation_create()
 * Acts when creating a new entity translation of a specific type.
 *
 * This hook runs after a new entity translation object has just been
 * instantiated.
 *
 * @ingroup entity_crud
 */
function tmx_tmx_map_translation_create(\Drupal\Core\Entity\EntityInterface $translation) {
  $a = 1; // TBD
}

/**
 * Implements hook_ENTITY_TYPE_translation_insert()
 * Respond to creation of a new entity translation of a particular type.
 *
 * This hook runs once the entity translation has been stored. Note that hook
 * implementations may not alter the stored entity translation data.
 *
 * @ingroup entity_crud
 */
function tmx_tmx_map_translation_insert(\Drupal\Core\Entity\EntityInterface $translation) {
  $a = 1; // TBD
}

/**
 * Implements hook_ENTITY_TYPE_translation_delete()
 * Respond to entity translation deletion of a particular type.
 *
 * This hook runs once the entity translation has been deleted from storage.
 *
 * @ingroup entity_crud
 */
function tmx_tmx_map_translation_delete(\Drupal\Core\Entity\EntityInterface $translation) {
  $a = 1; // TBD
}
