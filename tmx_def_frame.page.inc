<?php

/**
 * @file
 * Contains tmx_def_frame.page.inc.
 *
 * Page callback for TMX Frame Definition entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for TMX Frame Definition templates.
 *
 * Default template: tmx_def_frame.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_tmx_def_frame(array &$variables) {
  // Fetch TmxDefFrame Entity Object.
  $tmx_def_frame = $variables['elements']['#tmx_def_frame'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
