<?php

/**
 * @file
 * Contains tmx_def_wang_tile.page.inc.
 *
 * Page callback for TMX Wang Tile Definition entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for TMX Wang Tile Definition templates.
 *
 * Default template: tmx_def_wang_tile.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_tmx_def_wang_tile(array &$variables) {
  // Fetch TmxDefWangTile Entity Object.
  $tmx_def_wang_tile = $variables['elements']['#tmx_def_wang_tile'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
