<?php

/**
 * @file
 * Contains tmx_def_wang_set.page.inc.
 *
 * Page callback for TMX Wang Set Definition entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for TMX Wang Set Definition templates.
 *
 * Default template: tmx_def_wang_set.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_tmx_def_wang_set(array &$variables) {
  // Fetch TmxDefWangSet Entity Object.
  $tmx_def_wang_set = $variables['elements']['#tmx_def_wang_set'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
