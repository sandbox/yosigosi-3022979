<?php

/**
 * @file
 * Contains tmx_def_terrain.page.inc.
 *
 * Page callback for TMX Terrain Definition entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for TMX Terrain Definition templates.
 *
 * Default template: tmx_def_terrain.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_tmx_def_terrain(array &$variables) {
  // Fetch TmxDefTerrain Entity Object.
  $tmx_def_terrain = $variables['elements']['#tmx_def_terrain'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
