<?php

/**
 * @file
 * Contains tmx_def_tileitem.page.inc.
 *
 * Page callback for TMX Tileitem Definition entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for TMX Tileitem Definition templates.
 *
 * Default template: tmx_def_tileitem.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_tmx_def_tileitem(array &$variables) {
  // Fetch TmxDefTileitem Entity Object.
  $tmx_def_tileitem = $variables['elements']['#tmx_def_tileitem'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
