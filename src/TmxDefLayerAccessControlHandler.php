<?php

namespace Drupal\tmx;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the TMX Layer Definition entity.
 *
 * @see \Drupal\tmx\Entity\TmxDefLayer.
 */
class TmxDefLayerAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\tmx\Entity\TmxDefLayerInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished tmx layer definition entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published tmx layer definition entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit tmx layer definition entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete tmx layer definition entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add tmx layer definition entities');
  }

}
