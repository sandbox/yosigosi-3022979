<?php

namespace Drupal\tmx;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of TMX Coordinates Definition entities.
 *
 * @ingroup tmx
 */
class TmxDefCoordinatesListBuilder extends EntityListBuilder {


  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('TMX Coordinates Definition ID');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\tmx\Entity\TmxDefCoordinates */
    $row['id'] = $entity->id();
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.tmx_def_coordinates.edit_form',
      ['tmx_def_coordinates' => $entity->id()]
    );
    return $row + parent::buildRow($entity);
  }

}
