<?php

namespace Drupal\tmx\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting TMX Wang Color Definition entities.
 *
 * @ingroup tmx
 */
class TmxDefWangColorDeleteForm extends ContentEntityDeleteForm {


}
