<?php

namespace Drupal\tmx\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting TMX Template Definition entities.
 *
 * @ingroup tmx
 */
class TmxDefTemplateDeleteForm extends ContentEntityDeleteForm {


}
