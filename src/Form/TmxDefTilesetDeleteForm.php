<?php

namespace Drupal\tmx\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting TMX Tileset Definition entities.
 *
 * @ingroup tmx
 */
class TmxDefTilesetDeleteForm extends ContentEntityDeleteForm {


}
