<?php

namespace Drupal\tmx\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting TMX Object Definition entities.
 *
 * @ingroup tmx
 */
class TmxDefObjectDeleteForm extends ContentEntityDeleteForm {


}
