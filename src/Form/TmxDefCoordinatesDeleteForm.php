<?php

namespace Drupal\tmx\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting TMX Coordinates Definition entities.
 *
 * @ingroup tmx
 */
class TmxDefCoordinatesDeleteForm extends ContentEntityDeleteForm {


}
