<?php

namespace Drupal\tmx\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting TMX Map entities.
 *
 * @ingroup tmx
 */
class TmxMapDeleteForm extends ContentEntityDeleteForm {


}
