<?php

namespace Drupal\tmx\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting TMX Wang Tile Definition entities.
 *
 * @ingroup tmx
 */
class TmxDefWangTileDeleteForm extends ContentEntityDeleteForm {


}
