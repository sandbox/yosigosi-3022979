<?php

namespace Drupal\tmx\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting TMX Properties Definition entities.
 *
 * @ingroup tmx
 */
class TmxDefPropertiesDeleteForm extends ContentEntityDeleteForm {


}
