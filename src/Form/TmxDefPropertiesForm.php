<?php

namespace Drupal\tmx\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for TMX Properties Definition edit forms.
 *
 * @ingroup tmx
 */
class TmxDefPropertiesForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /* @var $entity \Drupal\tmx\Entity\TmxDefProperties */
    $form = parent::buildForm($form, $form_state);

    $entity = $this->entity;

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;

    $status = parent::save($form, $form_state);

    switch ($status) {
      case SAVED_NEW:
        drupal_set_message($this->t('Created the %label TMX Properties Definition.', [
          '%label' => $entity->label(),
        ]));
        break;

      default:
        drupal_set_message($this->t('Saved the %label TMX Properties Definition.', [
          '%label' => $entity->label(),
        ]));
    }
    $form_state->setRedirect('entity.tmx_def_properties.canonical', ['tmx_def_properties' => $entity->id()]);
  }

}
