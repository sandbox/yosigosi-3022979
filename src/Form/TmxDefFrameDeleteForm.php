<?php

namespace Drupal\tmx\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting TMX Frame Definition entities.
 *
 * @ingroup tmx
 */
class TmxDefFrameDeleteForm extends ContentEntityDeleteForm {


}
