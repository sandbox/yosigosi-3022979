<?php

namespace Drupal\tmx\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting TMX Map Definition entities.
 *
 * @ingroup tmx
 */
class TmxDefMapDeleteForm extends ContentEntityDeleteForm {


}
