<?php

namespace Drupal\tmx\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting TMX Terrain Definition entities.
 *
 * @ingroup tmx
 */
class TmxDefTerrainDeleteForm extends ContentEntityDeleteForm {


}
