<?php

namespace Drupal\tmx\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting TMX Chunk Definition entities.
 *
 * @ingroup tmx
 */
class TmxDefChunkDeleteForm extends ContentEntityDeleteForm {


}
