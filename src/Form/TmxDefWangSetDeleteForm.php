<?php

namespace Drupal\tmx\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting TMX Wang Set Definition entities.
 *
 * @ingroup tmx
 */
class TmxDefWangSetDeleteForm extends ContentEntityDeleteForm {


}
