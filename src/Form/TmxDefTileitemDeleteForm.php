<?php

namespace Drupal\tmx\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting TMX Tileitem Definition entities.
 *
 * @ingroup tmx
 */
class TmxDefTileitemDeleteForm extends ContentEntityDeleteForm {


}
