<?php

namespace Drupal\tmx\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting TMX Layer Definition entities.
 *
 * @ingroup tmx
 */
class TmxDefLayerDeleteForm extends ContentEntityDeleteForm {


}
