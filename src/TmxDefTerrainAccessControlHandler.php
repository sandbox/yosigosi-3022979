<?php

namespace Drupal\tmx;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the TMX Terrain Definition entity.
 *
 * @see \Drupal\tmx\Entity\TmxDefTerrain.
 */
class TmxDefTerrainAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\tmx\Entity\TmxDefTerrainInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished tmx terrain definition entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published tmx terrain definition entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit tmx terrain definition entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete tmx terrain definition entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add tmx terrain definition entities');
  }

}
