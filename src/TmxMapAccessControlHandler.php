<?php

namespace Drupal\tmx;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the TMX Map entity.
 *
 * @see \Drupal\tmx\Entity\TmxMap.
 */
class TmxMapAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\tmx\Entity\TmxMapInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished tmx map entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published tmx map entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit tmx map entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete tmx map entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add tmx map entities');
  }

}
