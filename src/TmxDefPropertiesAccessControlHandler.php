<?php

namespace Drupal\tmx;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the TMX Properties Definition entity.
 *
 * @see \Drupal\tmx\Entity\TmxDefProperties.
 */
class TmxDefPropertiesAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\tmx\Entity\TmxDefPropertiesInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished tmx properties definition entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published tmx properties definition entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit tmx properties definition entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete tmx properties definition entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add tmx properties definition entities');
  }

}
