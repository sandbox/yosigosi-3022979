<?php

namespace Drupal\tmx;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of TMX Wang Set Definition entities.
 *
 * @ingroup tmx
 */
class TmxDefWangSetListBuilder extends EntityListBuilder {


  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('TMX Wang Set Definition ID');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\tmx\Entity\TmxDefWangSet */
    $row['id'] = $entity->id();
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.tmx_def_wang_set.edit_form',
      ['tmx_def_wang_set' => $entity->id()]
    );
    return $row + parent::buildRow($entity);
  }

}
