<?php

namespace Drupal\tmx;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the TMX Object Definition entity.
 *
 * @see \Drupal\tmx\Entity\TmxDefObject.
 */
class TmxDefObjectAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\tmx\Entity\TmxDefObjectInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished tmx object definition entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published tmx object definition entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit tmx object definition entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete tmx object definition entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add tmx object definition entities');
  }

}
