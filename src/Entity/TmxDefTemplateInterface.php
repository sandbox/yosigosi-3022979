<?php

namespace Drupal\tmx\Entity;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Provides an interface for defining TMX Template Definition entities.
 *
 * @ingroup tmx
 */
interface TmxDefTemplateInterface extends ContentEntityInterface {

  // Add get/set methods for your configuration properties here.

  //  ref     | tmx_def_object | The object instantiated by this template (ref) (ref Object, original name: object)
  public function getDefObject();
  public function getDefObjectId();
  public function setDefObjectId($entity_id);
  public function setDefObject(Entity $entity);

  //  string  | type  | template file name
  public function getType();
  public function setType($value);

  //  computed | tileset | External tileset used by the template (optional) (ref Tileset)
  public function getTileset();
  public function setTileset($value);

}
