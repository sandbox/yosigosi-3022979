<?php

namespace Drupal\tmx\Entity;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Provides an interface for defining TMX Terrain Definition entities.
 *
 * @ingroup tmx
 */
interface TmxDefTerrainInterface extends ContentEntityInterface {

  // Add get/set methods for your configuration properties here.

  // int    | tmx_def_tileset | Referenced Tileset
  public function getDefTileset();
  public function getDefTilesetId();
  public function setDefTilesetId($entity_id);
  public function setDefTileset(Entity $entity);

  // int    | tmx_def_tileitem | Referenced Tileitem (Original: tile)
  public function getDefTileitem();
  public function getDefTileitemId();
  public function setDefTileitemId($entity_id);
  public function setDefTileitem(Entity $entity);

}
