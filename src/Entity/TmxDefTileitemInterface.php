<?php

namespace Drupal\tmx\Entity;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Provides an interface for defining TMX Tileitem Definition entities.
 *
 * @ingroup tmx
 */
interface TmxDefTileitemInterface extends ContentEntityInterface {

  // Add get/set methods for your configuration properties here.

  // int    | tmx_def_tileset | Referenced Tileset
  public function getDefTileset();
  public function getDefTilesetId();
  public function setDefTilesetId($entity_id);
  public function setDefTileset(Entity $entity);

  // int      | tmx_def_layer |  Layer with type objectgroup (optional) (Original: objectgroup)
  public function getDefLayer();
  public function getDefLayerId();
  public function setDefLayerId($entity_id);
  public function setDefLayer(Entity $entity);

  // int      | tid  |  Local ID of the tile (Original: id)
  public function getTid();
  public function setTid($value);

  // string   | type  |  The type of the tile (optional)
  public function getType();
  public function setType($value);

  // string   | image  |  Image representing this tile (optional)
  public function getImage();
  public function setImage($value);

  // ref       | tmx_img_fid |  Image used by this layer. imagelayer only.
  public function getImageFile();
  public function getImageFileId();
  public function setImageFileId($entity_id);
  public function setImageFile(Entity $entity);

  // int      | imagewidth  |  Width of the tile image in pixels
  public function getImageWidth();
  public function setImageWidth($value);

  // int      | imageheight  |  Height of the tile image in pixels
  public function getImageHeight();
  public function setImageHeight($value);



  // computed | properties |  A list of properties (name, value, type) (array)
  public function getProperties();
  public function setProperties($collection);
  public function setProperty($value);

  // computed | animation  |  Array of Frames (array)
  public function getFrames();
  public function setFrames($collection);
  public function setFrame($value);

  // computed | terrain    |  Index of terrain for each corner of tile (array)
  public function getTerrains();
  public function setTerrains($collection);
  public function setTerrain($value);

}
