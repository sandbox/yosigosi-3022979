<?php

namespace Drupal\tmx\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;

/**
 * Defines the TMX Chunk Definition entity.
 *
 * @ingroup tmx
 *
 * @ContentEntityType(
 *   id = "tmx_def_chunk",
 *   label = @Translation("TMX Chunk Definition"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\tmx\TmxDefChunkListBuilder",
 *     "views_data" = "Drupal\tmx\Entity\TmxDefChunkViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\tmx\Form\TmxDefChunkForm",
 *       "add" = "Drupal\tmx\Form\TmxDefChunkForm",
 *       "edit" = "Drupal\tmx\Form\TmxDefChunkForm",
 *       "delete" = "Drupal\tmx\Form\TmxDefChunkDeleteForm",
 *     },
 *     "access" = "Drupal\tmx\TmxDefChunkAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\tmx\TmxDefChunkHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "tmx_def_chunk",
 *   admin_permission = "administer tmx",
 *   entity_keys = {
 *     "id" = "tmx_cid",
 *   },
 *   links = {
 *     "canonical" = "/admin/content/tmx/tmx_def_chunk/{tmx_def_chunk}",
 *     "add-form" = "/admin/content/tmx/tmx_def_chunk/add",
 *     "edit-form" = "/admin/content/tmx/tmx_def_chunk/{tmx_def_chunk}/edit",
 *     "delete-form" = "/admin/content/tmx/tmx_def_chunk/{tmx_def_chunk}/delete",
 *     "collection" = "/admin/content/tmx/def_chunks",
 *   },
 *   field_ui_base_route = "tmx_def_chunk.settings"
 * )
 */
class TmxDefChunk extends ContentEntityBase implements TmxDefChunkInterface {

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    // int   | tmx_cid |  TMX Chunk Id

    // tmx_def_layer 	integer  Referenced Layer Definition
    $fields['tmx_def_layer'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Layer Definition'))
      ->setDescription(t('The Referenced Layer Definition this layer belongs.'))
      ->setSetting('target_type', 'tmx_def_map')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('view', [
          'label' => 'hidden',
          'type' => 'entity_reference_entity_id',
          'weight' => -1,
      ])
      ->setDisplayOptions('form', [
          'type' => 'entity_reference_autocomplete',
          'settings' => [
              'match_operator' => 'CONTAINS',
              'size' => '60',
              'autocomplete_type' => 'tags',
              'placeholder' => '',
          ],
          'weight' => -1,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE)
      ->setDefaultValue(0);

    // height 	int 	Height in tiles
    $fields['height'] = BaseFieldDefinition::create('integer')
    ->setLabel(t('Height (rows)'))
    ->setDescription(t('Row count. Same as map height for fixed-size maps.'))
    ->setSetting('size', 'normal')
    ->setSetting('unsigned', TRUE)
    ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'number_integer',
        'weight' => 5,
    ])
    ->setDisplayOptions('form', [
        'type' => 'number',
        'weight' => 5,
    ])
    ->setDisplayConfigurable('form', TRUE)
    ->setDisplayConfigurable('view', TRUE)
    ->setRequired(TRUE)
    ->setDefaultValue(0);


    // width 	int 	Width in tiles
    $fields['width'] = BaseFieldDefinition::create('integer')
    ->setLabel(t('Number of tile columns'))
    ->setDescription(t('Column count. Same as map width for fixed-size maps.'))
    ->setSetting('size', 'normal')
    ->setSetting('unsigned', TRUE)
    ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'number_integer',
        'weight' => 6,
    ])
    ->setDisplayOptions('form', [
        'type' => 'number',
        'weight' => 6,
    ])
    ->setDisplayConfigurable('form', TRUE)
    ->setDisplayConfigurable('view', TRUE)
    ->setRequired(TRUE)
    ->setDefaultValue(0);

    // x 	int 	X coordinate in tiles
    $fields['x'] = BaseFieldDefinition::create('integer')
    ->setLabel(t('X'))
    ->setDescription(t('Horizontal layer offset in tiles. Always 0.'))
    ->setSetting('size', 'normal')
    ->setSetting('unsigned', TRUE)
    ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'number_integer',
        'weight' => 9,
    ])
    ->setDisplayOptions('form', [
        'type' => 'number',
        'weight' => 9,
    ])
    ->setDisplayConfigurable('form', TRUE)
    ->setDisplayConfigurable('view', TRUE)
    ->setRequired(TRUE)
    ->setDefaultValue(0);

    // y 	int 	Y coordinate in tiles
    $fields['y'] = BaseFieldDefinition::create('integer')
    ->setLabel(t('Y'))
    ->setDescription(t('Vertical layer offset in tiles. Always 0.'))
    ->setSetting('size', 'normal')
    ->setSetting('unsigned', TRUE)
    ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'number_integer',
        'weight' => 10,
    ])
    ->setDisplayOptions('form', [
        'type' => 'number',
        'weight' => 10,
    ])
    ->setDisplayConfigurable('form', TRUE)
    ->setDisplayConfigurable('view', TRUE)
    ->setRequired(TRUE)
    ->setDefaultValue(0);

    // data 	Array of unsigned int (GIDs) or base64-encoded data
    $fields['data'] = BaseFieldDefinition::create('text_long')
    ->setLabel(t('Data'))
    ->setDescription(t('array or string 	Array of unsigned int (GIDs) or base64-encoded data.'))
    ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'text_default',
        'weight' => 14,
    ))
    ->setDisplayOptions('form', array(
        'type' => 'text_textfield',
        'weight' => 14,
    ))
    ->setDisplayConfigurable('view', TRUE)
    ->setDisplayConfigurable('form', TRUE)
    ->setRequired(FALSE)
    ->setDefaultValue('');

    return $fields;
  }

  // Referended Layer
  public function getDefLayer() {
    return $this->get('tmx_def_layer')->entity;
  }
  public function getDefLayerId() {
    return $this->get('tmx_def_layer')->target_id;
  }
  public function setDefLayerId($entity_id) {
    $this->set('tmx_def_layer', $entity_id);
    return $this;
  }
  public function setDefLayer(Entity $entity) {
    $this->set('tmx_def_layer', $entity->id());
    return $this;
  }


  // height 	int 	Height in tiles
  public function geHeight() {
    return $this->get('height')->value;
  }
  public function setHeight($value) {
    $this->set('height', $value);
    return $this;
  }

  // width 	int 	Width in tiles
  public function getWidth() {
    return $this->get('width')->value;
  }
  public function setWidth($value) {
    $this->set('width', $value);
    return $this;
  }


  // X coordinate in tiles
  public function getX() {
    return $this->get('x')->value;
  }
  public function setX($value) {
    $this->set('x', $value);
    return $this;
  }

  // Y coordinate in tiles
  public function getY() {
    return $this->get('y')->value;
  }
  public function setY($value) {
    $this->set('y', $value);
    return $this;
  }


  // data 	array or string 	Array of unsigned int (GIDs) or base64-encoded data.
  public function getData() {
    return $this->get('data')->value;
  }
  public function setData($value) {
    $this->set('data', $value);
    return $this;
  }

}
