<?php

namespace Drupal\tmx\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for TMX Layer Definition entities.
 */
class TmxDefLayerViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins, can be
    // put here.

    return $data;
  }

}
