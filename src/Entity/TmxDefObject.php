<?php

namespace Drupal\tmx\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;

/**
 * Defines the TMX Object Definition entity.
 *
 * @ingroup tmx
 *
 * @ContentEntityType(
 *   id = "tmx_def_object",
 *   label = @Translation("TMX Object Definition"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\tmx\TmxDefObjectListBuilder",
 *     "views_data" = "Drupal\tmx\Entity\TmxDefObjectViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\tmx\Form\TmxDefObjectForm",
 *       "add" = "Drupal\tmx\Form\TmxDefObjectForm",
 *       "edit" = "Drupal\tmx\Form\TmxDefObjectForm",
 *       "delete" = "Drupal\tmx\Form\TmxDefObjectDeleteForm",
 *     },
 *     "access" = "Drupal\tmx\TmxDefObjectAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\tmx\TmxDefObjectHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "tmx_def_object",
 *   admin_permission = "administer tmx",
 *   entity_keys = {
 *     "id" = "tmx_obid",
 *     "label" = "name",
 *   },
 *   links = {
 *     "canonical" = "/admin/content/tmx/tmx_def_object/{tmx_def_object}",
 *     "add-form" = "/admin/content/tmx/tmx_def_object/add",
 *     "edit-form" = "/admin/content/tmx/tmx_def_object/{tmx_def_object}/edit",
 *     "delete-form" = "/admin/content/tmx/tmx_def_object/{tmx_def_object}/delete",
 *     "collection" = "/admin/content/tmx/def_objects",
 *   },
 *   field_ui_base_route = "tmx_def_object.settings"
 * )
 */
class TmxDefObject extends ContentEntityBase implements TmxDefObjectInterface {

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    // int      | tmx_obid | TMX Object Id
    // string   | name  |  String assigned to name field in editor
    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the entity.'))
      ->setSettings([
          'max_length' => 50,
          'text_processing' => 0,
      ])
      ->setDisplayOptions('view', [
          'label' => 'above',
          'type' => 'string',
          'weight' => 0,
      ])
      ->setDisplayOptions('form', [
          'type' => 'string_textfield',
          'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE)
      ->setDefaultValue('');
    
    
    // int    | obid  |  Incremental id - unique across all objects
    $fields['obid'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Object id on map'))
      ->setDescription(t('Incremental id - unique across all layers.'))
      ->setSetting('size', 'normal')
      ->setSetting('unsigned', TRUE)
      ->setDisplayOptions('view', [
          'label' => 'inline',
          'type' => 'number_integer',
          'weight' => 1,
      ])
      ->setDisplayOptions('form', [
          'type' => 'number',
          'weight' => 1,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE)
      ->setDefaultValue(0);

    // string | type  |  String assigned to type field in editor
    $fields['type'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Type'))
      ->setDescription(t('String assigned to type field in editor.'))
      ->setSettings([
          'max_length' => 50,
          'text_processing' => 0,
      ])
      ->setDisplayOptions('view', [
          'label' => 'above',
          'type' => 'string',
          'weight' => 3,
      ])
      ->setDisplayOptions('form', [
          'type' => 'string_textfield',
          'weight' => 3,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE)
      ->setDefaultValue('');

    // double | rotation  |  Angle in degrees clockwise
    // int    | gid  |  GID, only if object comes from a Tilemap

    // string | template  |  Reference to a template file, in case object is a template instance
    $fields['template'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Template'))
      ->setDescription(t('Reference to a template file, in case object is a template instance.'))
      ->setSettings([
          'max_length' => 50,
          'text_processing' => 0,
      ])
      ->setDisplayOptions('view', [
          'label' => 'above',
          'type' => 'string',
          'weight' => 2,
      ])
      ->setDisplayOptions('form', [
          'type' => 'string_textfield',
          'weight' => 2,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE)
      ->setDefaultValue('');



    // object | text  |  String key-value pairs
    $fields['text'] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('Text'))
      ->setDescription(t('String key-value pairs (object).'))
      ->setDisplayOptions('view', array(
          'label' => 'above',
          'type' => 'text_default',
          'weight' => 14,
      ))
      ->setDisplayOptions('form', array(
          'type' => 'text_textfield',
          'weight' => 14,
      ))
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE)
      ->setRequired(FALSE)
      ->setDefaultValue('');




    // bool   | ellipse  |  Used to mark an object as an ellipse
    $fields['ellipse'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Ellipse'))
      ->setDescription(t('Used to mark an object as an ellipse.'))
      ->setDisplayOptions('view', [
          'label' => 'inline',
          'type' => 'boolean',
          'weight' => 4,
      ])
      ->setDisplayOptions('form', [
          'type' => 'boolean_checkbox',
          'settings' => [
              'display_label' => TRUE,
          ],
          'weight' => 4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE)
      ->setDefaultValue(TRUE);

    // bool   | point  |  Used to mark an object as a point
    $fields['point'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Point'))
      ->setDescription(t('Used to mark an object as a point.'))
      ->setDisplayOptions('view', [
          'label' => 'inline',
          'type' => 'boolean',
          'weight' => 4,
      ])
      ->setDisplayOptions('form', [
          'type' => 'boolean_checkbox',
          'settings' => [
              'display_label' => TRUE,
          ],
          'weight' => 4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE)
      ->setDefaultValue(TRUE);


    // bool   | visible  |  Whether object is shown in editor.
    $fields['visible'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Visible'))
      ->setDescription(t('A boolean indicating whether object is shown in editor.'))
      ->setDisplayOptions('view', [
          'label' => 'inline',
          'type' => 'boolean',
          'weight' => 4,
      ])
      ->setDisplayOptions('form', [
          'type' => 'boolean_checkbox',
          'settings' => [
              'display_label' => TRUE,
          ],
          'weight' => 4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE)
      ->setDefaultValue(TRUE);


    // double | height  |  Height in pixels. Ignored if using a gid.
    $fields['height'] = BaseFieldDefinition::create('integer')
    ->setLabel(t('Height (rows)'))
    ->setDescription(t('Row count. Same as map height for fixed-size maps.'))
    ->setSetting('size', 'normal')
    ->setSetting('unsigned', TRUE)
    ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'number_integer',
        'weight' => 5,
    ])
    ->setDisplayOptions('form', [
        'type' => 'number',
        'weight' => 5,
    ])
    ->setDisplayConfigurable('form', TRUE)
    ->setDisplayConfigurable('view', TRUE)
    ->setRequired(TRUE)
    ->setDefaultValue(0);

    // double | width  |  Width in pixels. Ignored if using a gid.
    $fields['width'] = BaseFieldDefinition::create('float')
    ->setLabel(t('Number of tile columns'))
    ->setDescription(t('Column count. Same as map width for fixed-size maps.'))
    ->setSetting('size', 'normal')
    ->setSetting('unsigned', TRUE)
    ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'number_integer',
        'weight' => 6,
    ])
    ->setDisplayOptions('form', [
        'type' => 'number',
        'weight' => 6,
    ])
    ->setDisplayConfigurable('form', TRUE)
    ->setDisplayConfigurable('view', TRUE)
    ->setRequired(TRUE)
    ->setDefaultValue(0);

    // double | x  |  X coordinate in pixels
    $fields['x'] = BaseFieldDefinition::create('float')
    ->setLabel(t('X'))
    ->setDescription(t('Horizontal layer offset in tiles. Always 0.'))
    ->setSetting('size', 'normal')
    ->setSetting('unsigned', TRUE)
    ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'number_integer',
        'weight' => 9,
    ])
    ->setDisplayOptions('form', [
        'type' => 'number',
        'weight' => 9,
    ])
    ->setDisplayConfigurable('form', TRUE)
    ->setDisplayConfigurable('view', TRUE)
    ->setRequired(TRUE)
    ->setDefaultValue(0);

    // double | y  |  Y coordinate in pixels
    $fields['y'] = BaseFieldDefinition::create('float')
    ->setLabel(t('Y'))
    ->setDescription(t('Vertical layer offset in tiles. Always 0.'))
    ->setSetting('size', 'normal')
    ->setSetting('unsigned', TRUE)
    ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'number_integer',
        'weight' => 10,
    ])
    ->setDisplayOptions('form', [
        'type' => 'number',
        'weight' => 10,
    ])
    ->setDisplayConfigurable('form', TRUE)
    ->setDisplayConfigurable('view', TRUE)
    ->setRequired(TRUE)
    ->setDefaultValue(0);

    return $fields;
  }

  // int    | obid  |  Incremental id - unique across all objects
  public function getObid() {
    return $this->get('obid')->value;
  }
  public function setObid($value) {
    $this->set('obid', $value);
    return $this;
  }

  // string | name  |  String assigned to name field in editor
  public function getName() {
    return $this->get('name')->value;
  }
  public function setName($value) {
    $this->set('name', $value);
    return $this;
  }

  // string | type  |  String assigned to type field in editor
  public function getType() {
    return $this->get('type')->value;
  }
  public function setType($value) {
    $this->set('type', $value);
    return $this;
  }

  // double | rotation  |  Angle in degrees clockwise
  public function getRotation() {
    return $this->get('rotation')->value;
  }
  public function setRotation($value) {
    $this->set('rotation', $value);
    return $this;
  }

  // int    | gid  |  GID, only if object comes from a Tilemap
  public function getGid() {
    return $this->get('gid')->value;
  }
  public function setGid($value) {
    $this->set('gid', $value);
    return $this;
  }

  // string | template  |  Reference to a template file, in case object is a template instance
  public function getTemplate() {
    return $this->get('template')->value;
  }
  public function setTemplate($value) {
    $this->set('template', $value);
    return $this;
  }

  // object | text  |  String key-value pairs
  public function getText() {
    return $this->get('text')->value;
  }
  public function setText($value) {
    $this->set('text', $value);
    return $this;
  }


  // bool   | ellipse  |  Used to mark an object as an ellipse
  public function isEllipse() {
    return (bool) $this->get('ellipse')->value;
  }
  public function setEllipse($value) {
    $this->set('ellipse', $value ? TRUE : FALSE);
    return $this;
  }

  // bool   | point  |  Used to mark an object as a point
  public function isPoint() {
    return (bool) $this->get('point')->value;
  }
  public function setPoint($value) {
    $this->set('visible', $value ? TRUE : FALSE);
    return $this;
  }


  // bool   | visible  |  Whether object is shown in editor.
  public function isVisible() {
    return (bool) $this->get('visible')->value;
  }
  public function setVisible($value) {
    $this->set('visible', $value ? TRUE : FALSE);
    return $this;
  }

  // double | height  |  Height in pixels. Ignored if using a gid.
  public function geHeight() {
    return $this->get('height')->value;
  }
  public function setHeight($value) {
    $this->set('height', $value);
    return $this;
  }

  // double | width  |  Width in pixels. Ignored if using a gid.
  public function getWidth() {
    return $this->get('width')->value;
  }
  public function setWidth($value) {
    $this->set('width', $value);
    return $this;
  }

  // double | x  |  X coordinate in pixels
  public function getX() {
    return $this->get('x')->value;
  }
  public function setX($value) {
    $this->set('x', $value);
    return $this;
  }

  // double | y  |  Y coordinate in pixels
  public function getY() {
    return $this->get('y')->value;
  }
  public function setY($value) {
    $this->set('y', $value);
    return $this;
  }

  // computed | polygon  |  A list of x,y coordinates in pixels (array)
  public function getPolygon() {
    return [];
  }

  // computed | polyline |  A list of x,y coordinates in pixels (array)
  public function getPolyline() {
    return [];
  }

  // computed | properties |  A list of properties (name, value, type) (array)
  public function getProperties() {
    return [];
  }


}
