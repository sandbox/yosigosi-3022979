<?php

namespace Drupal\tmx\Entity;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Provides an interface for defining TMX Map Definition entities.
 *
 * @ingroup tmx
 */
interface TmxDefMapInterface extends ContentEntityInterface {

  // Add get/set methods for your configuration properties here.
  
  // type 	string 	map (since 1.0)
  public function getType();
  public function setType($value);
  
  // orientation 	string 	orthogonal, isometric, staggered or hexagonal
  public function getOrientation();
  public function setOrientation($value);
  
  // version 	number 	The JSON format version
  public function getVersion();
  public function setVersion($value);
  
  // tiledversion 	string 	The Tiled version used to save the file
  public function getTiledVersion();
  public function setTiledVersion($value);
  
  // infinite 	bool 	Whether the map has infinite dimensions
  public function isInfinite();
  public function setInfinite($value);
  
  // height 	int 	Number of tile rows
  public function geHeight();
  public function setHeight($value);
  
  // width 	int 	Number of tile columns
  public function getWidth();
  public function setWidth($value);
  
  // tileheight 	int 	Map grid height
  public function getTileHeight();
  public function setTileHeight($value);
  
  // tilewidth 	int 	Map grid width
  public function getTileWidth();
  public function setTileWidth($value);
  
  // hexsidelength 	int 	Length of the side of a hex tile in pixels
  public function getHexSideLength();
  public function setHexSideLength($value);
  
  // backgroundcolor 	string 	Hex-formatted color (#RRGGBB or #AARRGGBB)
  // (optional)
  public function getBackgroundColor();
  public function setBackgroundColor($value);
  
  // renderorder 	string 	Rendering direction
  // orthogonal maps only
  public function getRenderOrder();
  public function setRenderOrder($value);
  
  // staggeraxis 	string 	x or y
  // staggered / hexagonal maps only
  public function getStaggerAxis();
  public function setStaggerAxis($value);
  
  // staggerindex 	string 	odd or even
  // staggered / hexagonal maps only
  public function getStaggerIndex();
  public function setStaggerIndex($value);
  
  // nextlayerid 	int 	Auto-increments for each layer
  public function getNextLayerId();
  public function setNextLayerId($value);
  
  // nextobjectid 	int 	Auto-increments for each placed object
  public function getNextObjectId();
  public function setNextObjectId($value);
  
  // properties 	array 	A list of properties (name, value, type).
  public function getProperties();
  
  // layers 	array 	Array of Layers
  public function getLayers();
  
  // tilesets 	array 	Array of Tilesets
  public function getTilesets();
  
}
