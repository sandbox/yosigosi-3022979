<?php

namespace Drupal\tmx\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;

/**
 * Defines the TMX Wang Color Definition entity.
 *
 * @ingroup tmx
 *
 * @ContentEntityType(
 *   id = "tmx_def_wang_color",
 *   label = @Translation("TMX Wang Color Definition"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\tmx\TmxDefWangColorListBuilder",
 *     "views_data" = "Drupal\tmx\Entity\TmxDefWangColorViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\tmx\Form\TmxDefWangColorForm",
 *       "add" = "Drupal\tmx\Form\TmxDefWangColorForm",
 *       "edit" = "Drupal\tmx\Form\TmxDefWangColorForm",
 *       "delete" = "Drupal\tmx\Form\TmxDefWangColorDeleteForm",
 *     },
 *     "access" = "Drupal\tmx\TmxDefWangColorAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\tmx\TmxDefWangColorHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "tmx_def_wang_color",
 *   admin_permission = "administer tmx",
 *   entity_keys = {
 *     "id" = "tmx_wcid",
 *     "label" = "name",
 *   },
 *   links = {
 *     "canonical" = "/admin/content/tmx/tmx_def_wang_color/{tmx_def_wang_color}",
 *     "add-form" = "/admin/content/tmx/tmx_def_wang_color/add",
 *     "edit-form" = "/admin/content/tmx/tmx_def_wang_color/{tmx_def_wang_color}/edit",
 *     "delete-form" = "/admin/content/tmx/tmx_def_wang_color/{tmx_def_wang_color}/delete",
 *     "collection" = "/admin/content/tmx/def_wang_colors",
 *   },
 *   field_ui_base_route = "tmx_def_wang_color.settings"
 * )
 */
class TmxDefWangColor extends ContentEntityBase implements TmxDefWangColorInterface {

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    // int    | tmx_wcid |  TMX Wang Color Id
    
    // string | name  |  Name of the Wang color
    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the entity.'))
      ->setSettings([
          'max_length' => 50,
          'text_processing' => 0,
      ])
      ->setDisplayOptions('view', [
          'label' => 'above',
          'type' => 'string',
          'weight' => 0,
      ])
      ->setDisplayOptions('form', [
          'type' => 'string_textfield',
          'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE)
      ->setDefaultValue('');
    
    // int    | tmx_def_wang_set | Referenced Wang Set
    $fields['tmx_def_wang_set'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Referenced Wang Set'))
      ->setDescription(t('Referenced Wang Set.'))
      ->setSetting('target_type', 'tmx_def_wang_set')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('view', [
          'label' => 'hidden',
          'type' => 'entity_reference_entity_id',
          'weight' => -1,
      ])
      ->setDisplayOptions('form', [
          'type' => 'entity_reference_autocomplete',
          'settings' => [
              'match_operator' => 'CONTAINS',
              'size' => '60',
              'autocomplete_type' => 'tags',
              'placeholder' => '',
          ],
          'weight' => -1,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE)
      ->setDefaultValue(0);

    // int    | tmx_def_tileitem | Referenced Tileitem (Original: tile)
    $fields['tmx_def_tileitem'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Referenced Tileitem'))
      ->setDescription(t('Referenced Tileitem (Original: tile).'))
      ->setSetting('target_type', 'tmx_def_tileset')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('view', [
          'label' => 'hidden',
          'type' => 'entity_reference_entity_id',
          'weight' => -1,
      ])
      ->setDisplayOptions('form', [
          'type' => 'entity_reference_autocomplete',
          'settings' => [
              'match_operator' => 'CONTAINS',
              'size' => '60',
              'autocomplete_type' => 'tags',
              'placeholder' => '',
          ],
          'weight' => -1,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE)
      ->setDefaultValue(0);

   // int    | tmx_def_wang_tile | Referenced Wang Tile
   $fields['tmx_def_wang_tile'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Referenced Wang Tile'))
      ->setDescription(t('Referenced Wang Tile.'))
      ->setSetting('target_type', 'tmx_def_wang_tile')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('view', [
          'label' => 'hidden',
          'type' => 'entity_reference_entity_id',
          'weight' => -1,
      ])
      ->setDisplayOptions('form', [
          'type' => 'entity_reference_autocomplete',
          'settings' => [
              'match_operator' => 'CONTAINS',
              'size' => '60',
              'autocomplete_type' => 'tags',
              'placeholder' => '',
          ],
          'weight' => -1,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE)
      ->setDefaultValue(0);

    // string | group | Group type of Wang colors (corner|edge)
    $fields['group'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Group type'))
      ->setDescription(t('Group type of Wang colors (corner|edge).'))
      ->setSettings([
          'max_length' => 50,
          'text_processing' => 0,
      ])
      ->setDisplayOptions('view', [
          'label' => 'above',
          'type' => 'string',
          'weight' => 2,
      ])
      ->setDisplayOptions('form', [
          'type' => 'string_textfield',
          'weight' => 2,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE)
      ->setDefaultValue('');

    // string | color |  Hex-formatted color (#RRGGBB or #AARRGGBB)
    $fields['color'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Color'))
      ->setDescription(t('Hex-formatted color (#RRGGBB or #AARRGGBB).'))
      ->setSettings([
          'max_length' => 50,
          'text_processing' => 0,
      ])
      ->setDisplayOptions('view', [
          'label' => 'above',
          'type' => 'string',
          'weight' => 2,
      ])
      ->setDisplayOptions('form', [
          'type' => 'string_textfield',
          'weight' => 2,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE)
      ->setDefaultValue('');

    // float  | probability |  Probability used when randomizing (double)
    $fields['probability'] = BaseFieldDefinition::create('float')
      ->setLabel(t('Probability'))
      ->setDescription(t('Probability used when randomizing (double).'))
      ->setSetting('size', 'normal')
      ->setSetting('unsigned', TRUE)
      ->setDisplayOptions('view', [
          'label' => 'inline',
          'type' => 'number_integer',
          'weight' => 9,
      ])
      ->setDisplayOptions('form', [
          'type' => 'number',
          'weight' => 9,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE)
      ->setDefaultValue(0);

    return $fields;
  }

  // int    | tmx_def_wang_set | Referenced Wang Set
  public function getDefWangSet() {
    return $this->get('tmx_def_wang_set')->entity;
  }
  public function getDefWangSetId() {
    return $this->get('tmx_def_wang_set')->target_id;
  }
  public function setDefWangSetId($entity_id) {
    $this->set('tmx_def_wang_set', $entity_id);
    return $this;
  }
  public function setDefWangSet(Entity $entity) {
    $this->set('tmx_def_wang_set', $entity->id());
    return $this;
  }

  // int    | tmx_def_tileitem | Referenced Tileitem (Original: tile)
  public function getDefTileitem() {
    return $this->get('tmx_def_tileitem')->entity;
  }
  public function getDefTileitemId() {
    return $this->get('tmx_def_tileitem')->target_id;
  }
  public function setDefTileitemId($entity_id) {
    $this->set('tmx_def_tileitem', $entity_id);
    return $this;
  }
  public function setDefTileitem(Entity $entity) {
    $this->set('tmx_def_tileitem', $entity->id());
    return $this;
  }

  // int    | tmx_def_wang_tile | Referenced Wang Tile
  public function getDefWangTile() {
    return $this->get('tmx_def_wang_tile')->entity;
  }
  public function getDefWangTileId() {
    return $this->get('tmx_def_wang_tile')->target_id;
  }
  public function setDefWangTileId($entity_id) {
    $this->set('tmx_def_wang_tile', $entity_id);
    return $this;
  }
  public function setDefWangTile(Entity $entity) {
    $this->set('tmx_def_wang_tile', $entity->id());
    return $this;
  }

  // string | group | Group type of Wang colors (corner|edge)
  public function getGroup() {
    return $this->get('group')->value;
  }
  public function setGroup($value) {
    $this->set('group', $value);
    return $this;
  }

  // string | color |  Hex-formatted color (#RRGGBB or #AARRGGBB)
  public function getColor() {
    return $this->get('color')->value;
  }
  public function setColor($value) {
    $this->set('color', $value);
    return $this;
  }

  // float  | probability |  Probability used when randomizing (double)
  public function getProbability() {
    return $this->get('probability')->value;
  }
  public function setProbability($value) {
    $this->set('probability', $value);
    return $this;
  }

}
