<?php

namespace Drupal\tmx\Entity;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Provides an interface for defining TMX Coordinates Definition entities.
 *
 * @ingroup tmx
 */
interface TmxDefCoordinatesInterface extends ContentEntityInterface {

  // Add get/set methods for your configuration properties here.

  // int    | tmx_groupid | Id of a group of coordinates, equals to first coordinate id in the group.
  public function getTmxGroupid();
  public function setTmxGroupid($value);

  // int    | crid | Row id
  public function getCrid();
  public function setCrid($value);

  // float | x  | X coordinate in pixels (double)
  public function getX();
  public function setX($value);

  // float | y  | Y coordinate in pixels (double)
  public function getY();
  public function setY($value);

}
