<?php

namespace Drupal\tmx\Entity;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Provides an interface for defining TMX Layer Definition entities.
 *
 * @ingroup tmx
 */
interface TmxDefLayerInterface extends ContentEntityInterface {

  // Add get/set methods for your configuration properties here.

  // tmx_def_map 	integer  Referenced Map Definition
  public function getDefMap();
  public function getDefMapId();
  public function setDefMapId($entity_id);
  public function setDefMap(Entity $entity);
  
  // tmx_def_layer 	integer  Image file id
  // (optional)
  public function getDefLayer();
  public function getDefLayerId();
  public function setDefLayerId($entity_id);
  public function setDefLayer(Entity $entity);
  
  // lid 	int 	Incremental id - unique across all layers
  public function getLid();
  public function setLid($value);
  
  // name 	string 	Name assigned to this layer
  public function getName();
  public function setName($value);
  
  // type 	string 	tilelayer, objectgroup, imagelayer or group
  public function getType();
  public function setType($value);
  
  // visible 	bool 	Whether layer is shown or hidden in editor
  public function isVisible();
  public function setVisible($value);
  
  // opacity 	double 	Value between 0 and 1
  public function getOpacity();
  public function setOpacity($value);
  
  // height 	int 	Row count. Same as map height for fixed-size maps.
  public function geHeight();
  public function setHeight($value);
  
  // width 	int 	Column count. Same as map width for fixed-size maps.
  public function getWidth();
  public function setWidth($value);
  
  // offsetx 	double 	Horizontal layer offset in pixels (default: 0)
  public function getOffsetX();
  public function setOffsetX($value);
  
  // offsety 	double 	Vertical layer offset in pixels (default: 0)
  public function getOffsetY();
  public function setOffsetY($value);
  
  // x 	int 	Horizontal layer offset in tiles. Always 0.
  public function getX();
  public function setX($value);
  
  // y 	int 	Vertical layer offset in tiles. Always 0.
  public function getY();
  public function setY($value);
  
  // image 	string 	Image used by this layer.
  // imagelayer only.
  public function getImage();
  public function setImage($value);
  
  // image_file 	integer  Image file id
  // imagelayer only.
  public function getImageFile();
  public function getImageFileId();
  public function setImageFileId($entity_id);
  public function setImageFile(Entity $entity);
  
  // transparentcolor 	string 	Hex-formatted color (#RRGGBB) (optional)
  // imagelayer only
  public function getTransparentColor();
  public function setTransparentColor($value);
  
  // data 	array or string 	Array of unsigned int (GIDs) or base64-encoded data.
  // tilelayer only.
  public function getData();
  public function setData($value);
  
  // compression 	string 	zlib, gzip or empty (default).
  // tilelayer only.
  public function getCompression();
  public function setCompression($value);
  
  // encoding 	string 	csv (default) or base64`.
  // tilelayer only.
  public function getEncoding();
  public function setEncoding($value);
  
  // draworder 	string 	topdown (default) or index. objectgroup only.
  public function getDrawOrder();
  public function setDrawOrder($value);
  
  // Array of properties objects (name, value, type).
  public function getProperties();
  
  // Array of Layer ids (optional).
  // group only.
  public function getLayers();
  
  // Array of Chunks ids.
  // tilelayer only
  public function getChunks();
  
  // objects 	object 	Array of objects.
  // objectgroup only.
  public function getObjects();
  
}
