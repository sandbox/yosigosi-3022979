<?php

namespace Drupal\tmx\Entity;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Provides an interface for defining TMX Chunk Definition entities.
 *
 * @ingroup tmx
 */
interface TmxDefChunkInterface extends ContentEntityInterface {

  // Add get/set methods for your configuration properties here.

  // Referended Layer
  public function getDefLayer();
  public function getDefLayerId();
  public function setDefLayerId($entity_id);
  public function setDefLayer(Entity $entity);

  // data 	array or string 	Array of unsigned int (GIDs) or base64-encoded data.
  // tilelayer only.
  public function getData();
  public function setData($value);
  
  // height 	int 	Row count. Same as map height for fixed-size maps.
  public function geHeight();
  public function setHeight($value);
  
  // width 	int 	Column count. Same as map width for fixed-size maps.
  public function getWidth();
  public function setWidth($value);
  
  
  // x 	int 	Horizontal layer offset in tiles. Always 0.
  public function getX();
  public function setX($value);
  
  // y 	int 	Vertical layer offset in tiles. Always 0.
  public function getY();
  public function setY($value);
}
