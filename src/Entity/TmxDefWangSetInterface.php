<?php

namespace Drupal\tmx\Entity;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Provides an interface for defining TMX Wang Set Definition entities.
 *
 * @ingroup tmx
 */
interface TmxDefWangSetInterface extends ContentEntityInterface {

  // Add get/set methods for your configuration properties here.

  // int    | tmx_def_tileset | Referenced Tileset
  public function getDefTileset();
  public function getDefTilesetId();
  public function setDefTilesetId($entity_id);
  public function setDefTileset(Entity $entity);

  // int    | tmx_def_tileitem | Referenced Tileitem (Original: tile)
  public function getDefTileitem();
  public function getDefTileitemId();
  public function setDefTileitemId($entity_id);
  public function setDefTileitem(Entity $entity);

  // computed | cornercolors | Array of Wang colors (array).
  public function getCornerColors();
  public function setCornerColors($collection);
  public function setCornerColor($value);

  // computed | edgecolors | Array of Wang colors (array).
  public function getEdgeColors();
  public function setEdgeColors($collection);
  public function setEdgeColor($value);

  // computed | wangtiles | Array of Wang tiles (array).
  public function getWangTiles();
  public function setWangTiles($collection);
  public function setWangTile($value);

}
