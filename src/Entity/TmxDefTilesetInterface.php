<?php

namespace Drupal\tmx\Entity;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Provides an interface for defining TMX Tileset Definition entities.
 *
 * @ingroup tmx
 */
interface TmxDefTilesetInterface extends ContentEntityInterface {

  // Add get/set methods for your configuration properties here.

  // ref      | tmx_def_map | Referended Map
  public function getDefMap();
  public function getDefMapId();
  public function setDefMapId($entity_id);
  public function setDefMap(Entity $entity);

  // ref      | tmx_def_layer | Referended Template
  public function getDefTemplate();
  public function getDefTemplateId();
  public function setDefTemplateId($entity_id);
  public function setDefTemplate(Entity $entity);

  // string   | name  |  Name given to this tileset
  public function getName();
  public function setName($value);

  // string   | type  |  tileset (for tileset files, since 1.0)
  public function getType();
  public function setType($value);

  // int      | columns  |  The number of tile columns in the tileset
  public function getColumns();
  public function setColumns($value);

  // int      | margin  |  Buffer between image edge and first tile (pixels)
  public function getMargin();
  public function setMargin($value);

  // int      | spacing  |  Spacing between adjacent tiles in image (pixels)
  public function getSpacing();
  public function setSpacing($value);

  // int      | tilecount  |  The number of tiles in this tileset
  public function getTileCount();
  public function setTileCount($value);

  // int      | tileheight  |  Maximum height of tiles in this set
  public function getTileHeight();
  public function setTileHeight($value);

  // int      | tilewidth  |  Maximum width of tiles in this set
  public function getTileWidth();
  public function setTileWidth($value);

  // int      | firstgid  |  GID corresponding to the first tile in the set
  public function getFirstGid();
  public function setFirstGid($value);

  // string   | image  |  Image used for tiles in this set
  public function getImage();
  public function setImage($value);

  // ref       | tmx_img_fid |  Image used by this layer. imagelayer only.
  public function getImageFile();
  public function getImageFileId();
  public function setImageFileId($entity_id);
  public function setImageFile(Entity $entity);

  // int      | imagewidth  |  Width of source image in pixels
  public function getImageWidth();
  public function setImageWidth($value);

  // int      | imageheight  |  Height of source image in pixels
  public function getImageHeight();
  public function setImageHeight($value);

  // string   | transparentcolor  |  Hex-formatted color (#RRGGBB) (optional)
  public function getTransparentColor();
  public function setTransparentColor($value);

  // string   | gridtype  | Grid. Orientation of the grid for the tiles in this tileset (orthogonal or isometric). only isometric (optional)
  public function getGridType();
  public function setGridType($value);

  // int      | gridwidth | Grid. Width of a grid cell (optional)
  public function getGridWidth();
  public function setGridWidth($value);

  // int      | gridheight | Grid. Height of a grid cell (optional)
  public function getGridHeight();
  public function setGridHeight($value);

  // int      | tileoffsetx | Tileoffset. Horizontal offset in pixels (optional).
  public function getTileOffsetX();
  public function setTileOffsetX($value);

  // int      | tileoffsety | Tileoffset. Vertical offset in pixels (positive is down) (optional).
  public function getTileOffsetY();
  public function setTileOffsetY($value);

  // computed | properties |  A list of properties (name, value, type) (array).
  public function getProperties();
  public function setProperties($collection);
  public function setProperty($value);

  // computed | terrains |  Array of Terrains (optional) (array).
  public function getTerrains();
  public function setTerrains($collection);
  public function setTerrain($value);

  // computed | tiles | Array of Tiles (optional) (array).
  public function getTiles();
  public function setTiles($collection);
  public function setTile($value);

  // computed | wangsets | Array of Wang sets (since 1.1.5) (array).
  public function getWangsets();
  public function setWangsets($collection);
  public function setWangset($value);

  // computed | grid | Grid. (optional) Build from fields: gridwidth(width), gridheight(height), gridtype(orientation)
  public function getGrid();
  public function setGrid($value);

  // computed | tileoffset | Tileoffset. (optional) (object) Built from: tileoffsetx(x), tileoffsety(y)
  public function getTileoffset();
  public function setTileoffset($value);

}
