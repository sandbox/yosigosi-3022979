<?php

namespace Drupal\tmx\Entity;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Provides an interface for defining TMX Wang Color Definition entities.
 *
 * @ingroup tmx
 */
interface TmxDefWangColorInterface extends ContentEntityInterface {

  // Add get/set methods for your configuration properties here.

  // int    | tmx_def_wang_set | Referenced Wang Set
  public function getDefWangSet();
  public function getDefWangSetId();
  public function setDefWangSetId($entity_id);
  public function setDefWangSet(Entity $entity);

  // int    | tmx_def_tileitem | Referenced Tileitem (Original: tile)
  public function getDefTileitem();
  public function getDefTileitemId();
  public function setDefTileitemId($entity_id);
  public function setDefTileitem(Entity $entity);

  // int    | tmx_def_wang_tile | Referenced Wang Tile
  public function getDefWangTile();
  public function getDefWangTileId();
  public function setDefWangTileId($entity_id);
  public function setDefWangTile(Entity $entity);

  // string | group | Group type of Wang colors (corner|edge)
  public function getGroup();
  public function setGroup($value);

  // string | color |  Hex-formatted color (#RRGGBB or #AARRGGBB)
  public function getColor();
  public function setColor($value);

  // float  | probability |  Probability used when randomizing (double)
  public function getProbability();
  public function setProbability($value);

}
