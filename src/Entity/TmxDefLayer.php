<?php

namespace Drupal\tmx\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;

/**
 * Defines the TMX Layer Definition entity.
 *
 * @ingroup tmx
 *
 * @ContentEntityType(
 *   id = "tmx_def_layer",
 *   label = @Translation("TMX Layer Definition"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\tmx\TmxDefLayerListBuilder",
 *     "views_data" = "Drupal\tmx\Entity\TmxDefLayerViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\tmx\Form\TmxDefLayerForm",
 *       "add" = "Drupal\tmx\Form\TmxDefLayerForm",
 *       "edit" = "Drupal\tmx\Form\TmxDefLayerForm",
 *       "delete" = "Drupal\tmx\Form\TmxDefLayerDeleteForm",
 *     },
 *     "access" = "Drupal\tmx\TmxDefLayerAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\tmx\TmxDefLayerHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "tmx_def_layer",
 *   admin_permission = "administer tmx",
 *   entity_keys = {
 *     "id" = "tmx_lid",
 *     "name" = "name",
 *   },
 *   links = {
 *     "canonical" = "/admin/content/tmx/tmx_def_layer/{tmx_def_layer}",
 *     "add-form" = "/admin/content/tmx/tmx_def_layer/add",
 *     "edit-form" = "/admin/content/tmx/tmx_def_layer/{tmx_def_layer}/edit",
 *     "delete-form" = "/admin/content/tmx/tmx_def_layer/{tmx_def_layer}/delete",
 *     "collection" = "/admin/content/tmx/def_layers",
 *   },
 *   field_ui_base_route = "tmx_def_layer.settings"
 * )
 */
class TmxDefLayer extends ContentEntityBase implements TmxDefLayerInterface {

  /**
   * {@inheritdoc}
   * @see: core/lib/Drupal/Core/Field/Plugin/Field
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    // int       | tmx_lid | TMX Map Id
    // string    | name  |  Name assigned to this layer

      // tmx_def_map 	integer  Referenced Map Definition
    $fields['tmx_def_map'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Map Definition'))
      ->setDescription(t('The Referenced Map Definition this layer belongs.'))
      ->setSetting('target_type', 'tmx_def_map')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('view', [
          'label' => 'hidden',
          'type' => 'entity_reference_entity_id',
          'weight' => -2,
      ])
      ->setDisplayOptions('form', [
          'type' => 'entity_reference_autocomplete',
          'settings' => [
              'match_operator' => 'CONTAINS',
              'size' => '60',
              'autocomplete_type' => 'tags',
              'placeholder' => '',
          ],
          'weight' => -2,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE)
      ->setDefaultValue(0);

    // tmx_def_layer 	integer  Referenced Layer Definition (optional)
    $fields['tmx_def_layer'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Layer Definition'))
      ->setDescription(t('The Referenced Layer Definition this layer belongs.'))
      ->setSetting('target_type', 'tmx_def_map')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('view', [
          'label' => 'hidden',
          'type' => 'entity_reference_entity_id',
          'weight' => -1,
      ])
      ->setDisplayOptions('form', [
          'type' => 'entity_reference_autocomplete',
          'settings' => [
              'match_operator' => 'CONTAINS',
              'size' => '60',
              'autocomplete_type' => 'tags',
              'placeholder' => '',
          ],
          'weight' => -1,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE)
      ->setDefaultValue(0);

    // lid 	int 	Incremental id - unique across all layers
    $fields['lid'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Layer id on map'))
      ->setDescription(t('Incremental id - unique across all layers.'))
      ->setSetting('size', 'normal')
      ->setSetting('unsigned', TRUE)
      ->setDisplayOptions('view', [
          'label' => 'inline',
          'type' => 'number_integer',
          'weight' => 1,
      ])
      ->setDisplayOptions('form', [
          'type' => 'number',
          'weight' => 1,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE)
      ->setDefaultValue(0);

    // type 	string 	tilelayer, objectgroup, imagelayer or group
    $fields['type'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Type'))
      ->setDescription(t('Type (tilelayer|objectgroup|imagelayer|group).'))
      ->setSettings([
          'max_length' => 50,
          'text_processing' => 0,
      ])
      ->setDisplayOptions('view', [
          'label' => 'above',
          'type' => 'string',
          'weight' => 3,
      ])
      ->setDisplayOptions('form', [
          'type' => 'string_textfield',
          'weight' => 3,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE)
      ->setDefaultValue('tilelayer');

    // visible 	bool 	Whether layer is shown or hidden in editor
    $fields['visible'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Visible'))
      ->setDescription(t('A boolean indicating whether the layer is shown or hidden in editor.'))
      ->setDisplayOptions('view', [
          'label' => 'inline',
          'type' => 'boolean',
          'weight' => 4,
      ])
      ->setDisplayOptions('form', [
          'type' => 'boolean_checkbox',
          'settings' => [
              'display_label' => TRUE,
          ],
          'weight' => 4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE)
      ->setDefaultValue(TRUE);

      // opacity 	double 	Value between 0 and 1
    $fields['opacity'] = BaseFieldDefinition::create('float')
      ->setLabel(t('Offset X'))
      ->setDescription(t('Value between 0 and 1.'))
      ->setSettings([
          'min' => 0,
          'max' => 1,
          'size' => 'big',
          'unsigned' => TRUE,
      ])
      ->setDisplayOptions('view', [
          'label' => 'above',
          'type' => 'number_decimal',
          'weight' => 7,
      ])
      ->setDisplayOptions('form', [
          'type' => 'decimal',
          'weight' => 7,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE)
      ->setDefaultValue(1);

    // height 	int 	Row count. Same as map height for fixed-size maps.
    $fields['height'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Height (rows)'))
      ->setDescription(t('Row count. Same as map height for fixed-size maps.'))
      ->setSetting('size', 'normal')
      ->setSetting('unsigned', TRUE)
      ->setDisplayOptions('view', [
          'label' => 'inline',
          'type' => 'number_integer',
          'weight' => 5,
      ])
      ->setDisplayOptions('form', [
          'type' => 'number',
          'weight' => 5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE)
      ->setDefaultValue(0);


    // width 	int 	Column count. Same as map width for fixed-size maps.
    $fields['width'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Number of tile columns'))
      ->setDescription(t('Column count. Same as map width for fixed-size maps.'))
      ->setSetting('size', 'normal')
      ->setSetting('unsigned', TRUE)
      ->setDisplayOptions('view', [
          'label' => 'inline',
          'type' => 'number_integer',
          'weight' => 6,
      ])
      ->setDisplayOptions('form', [
          'type' => 'number',
          'weight' => 6,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE)
      ->setDefaultValue(0);

    // offsetx 	double 	Horizontal layer offset in pixels (default: 0)
    $fields['offsetx'] = BaseFieldDefinition::create('float')
      ->setLabel(t('Offset X'))
      ->setDescription(t('Horizontal layer offset in pixels.'))
      ->setSettings([
          'min' => 0,
          'size' => 'big',
          'unsigned' => TRUE,
      ])
      ->setDisplayOptions('view', [
          'label' => 'above',
          'type' => 'number_decimal',
          'weight' => 7,
      ])
      ->setDisplayOptions('form', [
          'type' => 'decimal',
          'weight' => 7,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE)
      ->setDefaultValue(0);

    // offsety 	double 	Vertical layer offset in pixels (default: 0)
    $fields['offsety'] = BaseFieldDefinition::create('float')
      ->setLabel(t('Offset Y'))
      ->setDescription(t('Vertical layer offset in pixels.'))
      ->setSettings([
          'min' => 0,
          'size' => 'big',
          'unsigned' => TRUE,
      ])
      ->setDisplayOptions('view', [
          'label' => 'above',
          'type' => 'number_decimal',
          'weight' => 8,
      ])
      ->setDisplayOptions('form', [
          'type' => 'decimal',
          'weight' => 8,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE)
      ->setDefaultValue(0);


    // x 	int 	Horizontal layer offset in tiles. Always 0.
    $fields['x'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('X'))
      ->setDescription(t('Horizontal layer offset in tiles. Always 0.'))
      ->setSetting('size', 'normal')
      ->setSetting('unsigned', TRUE)
      ->setDisplayOptions('view', [
          'label' => 'inline',
          'type' => 'number_integer',
          'weight' => 9,
      ])
      ->setDisplayOptions('form', [
          'type' => 'number',
          'weight' => 9,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE)
      ->setDefaultValue(0);

    // y 	int 	Vertical layer offset in tiles. Always 0.
    $fields['y'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Y'))
      ->setDescription(t('Vertical layer offset in tiles. Always 0.'))
      ->setSetting('size', 'normal')
      ->setSetting('unsigned', TRUE)
      ->setDisplayOptions('view', [
          'label' => 'inline',
          'type' => 'number_integer',
          'weight' => 10,
      ])
      ->setDisplayOptions('form', [
          'type' => 'number',
          'weight' => 10,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE)
      ->setDefaultValue(0);

    // image 	string 	Image used by this layer.
    // imagelayer only.
    $fields['image'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Image'))
      ->setDescription(t('Image used by this layer.'))
      ->setSettings([
          'max_length' => 50,
          'text_processing' => 0,
      ])
      ->setDisplayOptions('view', [
          'label' => 'above',
          'type' => 'string',
          'weight' => 11,
      ])
      ->setDisplayOptions('form', [
          'type' => 'string_textfield',
          'weight' => 11,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(FALSE)
      ->setDefaultValue('');

    // tmx_img_fid 	integer  Image file id
    // imagelayer only.
    $fields['tmx_img_fid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Image file id'))
      ->setDescription(t('The Image file id that implements the field image.'))
      ->setSetting('target_type', 'file')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('view', [
          'label' => 'hidden',
          'type' => 'entity_reference_label',
          'weight' => 12,
      ])
      ->setDisplayOptions('form', [
          'type' => 'entity_reference_autocomplete',
          'settings' => [
              'match_operator' => 'CONTAINS',
              'size' => '60',
              'autocomplete_type' => 'tags',
              'placeholder' => '',
          ],
          'weight' => 12,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE)
      ->setDefaultValue(0);

    // transparentcolor 	string 	Hex-formatted color (#RRGGBB)
    // (optional)
    // imagelayer only
    $fields['transparentcolor'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Transparent color'))
      ->setDescription(t('Hex-formatted color (#RRGGBB).'))
      ->setSettings([
          'max_length' => 50,
          'text_processing' => 0,
      ])
      ->setDisplayOptions('view', [
          'label' => 'above',
          'type' => 'string',
          'weight' => 13,
      ])
      ->setDisplayOptions('form', [
          'type' => 'string_textfield',
          'weight' => 13,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(FALSE)
      ->setDefaultValue('');

    // data 	array or string 	Array of unsigned int (GIDs) or base64-encoded data.
    // tilelayer only.
    $fields['data'] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('Data'))
      ->setDescription(t('array or string 	Array of unsigned int (GIDs) or base64-encoded data.'))
      ->setDisplayOptions('view', array(
          'label' => 'above',
          'type' => 'text_default',
          'weight' => 14,
      ))
      ->setDisplayOptions('form', array(
          'type' => 'text_textfield',
          'weight' => 14,
      ))
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE)
      ->setRequired(FALSE)
      ->setDefaultValue('');

    // compression 	string 	zlib, gzip or empty (default).
    // tilelayer only.
    $fields['compression'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Compression'))
      ->setDescription(t('zlib, gzip or empty (default).'))
      ->setSettings([
          'max_length' => 4,
          'text_processing' => 0,
      ])
      ->setDisplayOptions('view', [
          'label' => 'above',
          'type' => 'string',
          'weight' => 15,
      ])
      ->setDisplayOptions('form', [
          'type' => 'string_textfield',
          'weight' => 15,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(FALSE)
      ->setDefaultValue('');

    // encoding 	string 	csv (default) or base64`.
    // tilelayer only.
    $fields['encoding'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Encoding'))
      ->setDescription(t('csv (default) or base64.'))
      ->setSettings([
          'max_length' => 6,
          'text_processing' => 0,
      ])
      ->setDisplayOptions('view', [
          'label' => 'above',
          'type' => 'string',
          'weight' => 16,
      ])
      ->setDisplayOptions('form', [
          'type' => 'string_textfield',
          'weight' => 16,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(FALSE)
      ->setDefaultValue('');

    // draworder 	string 	topdown (default) or index.
    // objectgroup only.
    $fields['draworder'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Draw order'))
      ->setDescription(t('topdown (default) or index.'))
      ->setSettings([
          'max_length' => 6,
          'text_processing' => 0,
      ])
      ->setDisplayOptions('view', [
          'label' => 'above',
          'type' => 'string',
          'weight' => 17,
      ])
      ->setDisplayOptions('form', [
          'type' => 'string_textfield',
          'weight' => 17,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(FALSE)
      ->setDefaultValue('');

    return $fields;
  }

  // tmx_def_map 	integer  Referenced Map Definition
  public function getDefMap() {
    return $this->get('tmx_def_map')->entity;
  }
  public function getDefMapId() {
    return $this->get('tmx_def_map')->target_id;
  }
  public function setDefMapId($entity_id) {
    $this->set('tmx_def_map', $entity_id);
    return $this;
  }
  public function setDefMap(Entity $entity) {
    $this->set('tmx_def_map', $entity->id());
    return $this;
  }

  // tmx_def_layer 	integer  Image file id
  // (optional)
  public function getDefLayer() {
    return $this->get('tmx_def_layer')->entity;
  }
  public function getDefLayerId() {
    return $this->get('tmx_def_layer')->target_id;
  }
  public function setDefLayerId($entity_id) {
    $this->set('tmx_def_layer', $entity_id);
    return $this;
  }
  public function setDefLayer(Entity $entity) {
    $this->set('tmx_def_layer', $entity->id());
    return $this;
  }



  // lid 	int 	Incremental id - unique across all layers
  public function getLid() {
    return $this->get('lid')->value;
  }
  public function setLid($value) {
    $this->set('lid', $value);
    return $this;
  }

  // name 	string 	Name assigned to this layer
  public function getName() {
    return $this->get('name')->value;
  }
  public function setName($value) {
    $this->set('name', $value);
    return $this;
  }

  // type 	string 	tilelayer, objectgroup, imagelayer or group
  public function getType() {
    return $this->get('type')->value;
  }
  public function setType($value) {
    $this->set('type', $value);
    return $this;
  }

  // visible 	bool 	Whether layer is shown or hidden in editor
  public function isVisible() {
    return (bool) $this->get('visible')->value;
  }
  public function setVisible($value) {
    $this->set('visible', $value ? TRUE : FALSE);
    return $this;
  }

  // opacity 	double 	Value between 0 and 1
  public function getOpacity() {
    return $this->get('opacity')->value;
  }
  public function setOpacity($value) {
    $this->set('opacity', $value);
    return $this;
  }

  // height 	int 	Row count. Same as map height for fixed-size maps.
  public function geHeight() {
    return $this->get('height')->value;
  }
  public function setHeight($value) {
    $this->set('height', $value);
    return $this;
  }

  // width 	int 	Column count. Same as map width for fixed-size maps.
  public function getWidth() {
    return $this->get('width')->value;
  }
  public function setWidth($value) {
    $this->set('width', $value);
    return $this;
  }

  // offsetx 	double 	Horizontal layer offset in pixels (default: 0)
  public function getOffsetX() {
    return $this->get('offsetx')->value;
  }
  public function setOffsetX($value) {
    $this->set('offsetx', $value);
    return $this;
  }

  // offsety 	double 	Vertical layer offset in pixels (default: 0)
  public function getOffsetY() {
    return $this->get('offsety')->value;
  }
  public function setOffsetY($value) {
    $this->set('offsety', $value);
    return $this;
  }

  // x 	int 	Horizontal layer offset in tiles. Always 0.
  public function getX() {
    return $this->get('x')->value;
  }
  public function setX($value) {
    $this->set('x', $value);
    return $this;
  }

  // y 	int 	Vertical layer offset in tiles. Always 0.
  public function getY() {
    return $this->get('y')->value;
  }
  public function setY($value) {
    $this->set('y', $value);
    return $this;
  }

  // image 	string 	Image used by this layer.
  // imagelayer only.
  public function getImage() {
    return $this->get('image')->value;
  }
  public function setImage($value) {
    $this->set('image', $value);
    return $this;
  }

  // image_file 	integer  Image file id
  // imagelayer only.
  public function getImageFile() {
    return $this->get('tmx_img_fid')->entity;
  }
  public function getImageFileId() {
    return $this->get('tmx_img_fid')->target_id;
  }
  public function setImageFileId($entity_id) {
    $this->set('tmx_img_fid', $entity_id);
    return $this;
  }
  public function setImageFile(Entity $entity) {
    $this->set('tmx_img_fid', $entity->id());
    return $this;
  }

  // transparentcolor 	string 	Hex-formatted color (#RRGGBB) (optional)
  // imagelayer only
  public function getTransparentColor() {
    return $this->get('transparentcolor')->value;
  }
  public function setTransparentColor($value) {
    $this->set('transparentcolor', $value);
    return $this;
  }

  // data 	array or string 	Array of unsigned int (GIDs) or base64-encoded data.
  // tilelayer only.
  public function getData() {
    return $this->get('data')->value;
  }
  public function setData($value) {
    $this->set('data', $value);
    return $this;
  }

  // compression 	string 	zlib, gzip or empty (default).
  // tilelayer only.
  public function getCompression() {
    return $this->get('compression')->value;
  }
  public function setCompression($value) {
    $this->set('compression', $value);
    return $this;
  }

  // encoding 	string 	csv (default) or base64`.
  // tilelayer only.
  public function getEncoding() {
    return $this->get('encoding')->value;
  }
  public function setEncoding($value) {
    $this->set('encoding', $value);
    return $this;
  }

  // draworder 	string 	topdown (default) or index. objectgroup only.
  public function getDrawOrder() {
    return $this->get('draworder')->value;
  }
  public function setDrawOrder($value) {
    $this->set('draworder', $value);
    return $this;
  }

  // Array of properties objects (name, value, type).
  public function getProperties() {
    return [];
  }

  // Array of Layer ids (optional).
  // group only.
  public function getLayers() {
    return [];
  }

  // Array of Chunks ids.
  // tilelayer only
  public function getChunks() {
    return [];
  }

  // objects 	object 	Array of objects.
  // objectgroup only.
  public function getObjects() {
    return [];
  }


}
