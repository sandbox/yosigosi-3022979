<?php

namespace Drupal\tmx\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for TMX Coordinates Definition entities.
 */
class TmxDefCoordinatesViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins, can be
    // put here.

    return $data;
  }

}
