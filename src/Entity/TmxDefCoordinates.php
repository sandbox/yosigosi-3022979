<?php

namespace Drupal\tmx\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;

/**
 * Defines the TMX Coordinates Definition entity.
 *
 * @ingroup tmx
 *
 * @ContentEntityType(
 *   id = "tmx_def_coordinates",
 *   label = @Translation("TMX Coordinates Definition"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\tmx\TmxDefCoordinatesListBuilder",
 *     "views_data" = "Drupal\tmx\Entity\TmxDefCoordinatesViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\tmx\Form\TmxDefCoordinatesForm",
 *       "add" = "Drupal\tmx\Form\TmxDefCoordinatesForm",
 *       "edit" = "Drupal\tmx\Form\TmxDefCoordinatesForm",
 *       "delete" = "Drupal\tmx\Form\TmxDefCoordinatesDeleteForm",
 *     },
 *     "access" = "Drupal\tmx\TmxDefCoordinatesAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\tmx\TmxDefCoordinatesHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "tmx_def_coordinates",
 *   admin_permission = "administer tmx",
 *   entity_keys = {
 *     "id" = "tmx_crid",
 *   },
 *   links = {
 *     "canonical" = "/admin/content/tmx/tmx_def_coordinates/{tmx_def_coordinates}",
 *     "add-form" = "/admin/content/tmx/tmx_def_coordinates/add",
 *     "edit-form" = "/admin/content/tmx/tmx_def_coordinates/{tmx_def_coordinates}/edit",
 *     "delete-form" = "/admin/content/tmx/tmx_def_coordinates/{tmx_def_coordinates}/delete",
 *     "collection" = "/admin/content/tmx/tmx_def_coordinates",
 *   },
 *   field_ui_base_route = "tmx_def_coordinates.settings"
 * )
 */
class TmxDefCoordinates extends ContentEntityBase implements TmxDefCoordinatesInterface {

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    // int    | tmx_crid |  TMX Coordinates Id

    // int    | tmx_groupid | Id of a group of coordinates, equals to first coordinate id in the group.
    $fields['tmx_groupid'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Tmx Groupid'))
      ->setDescription(t('Id of a group of coordinates, equals to first coordinate id in the group.'))
      ->setSetting('size', 'normal')
      ->setSetting('unsigned', TRUE)
      ->setDisplayOptions('view', [
          'label' => 'inline',
          'type' => 'number_integer',
          'weight' => 9,
      ])
      ->setDisplayOptions('form', [
          'type' => 'number',
          'weight' => 9,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE)
      ->setDefaultValue(0);


    // int    | crid | Row id
    $fields['crid'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Id'))
      ->setDescription(t('Row id.'))
      ->setSetting('size', 'normal')
      ->setSetting('unsigned', TRUE)
      ->setDisplayOptions('view', [
          'label' => 'inline',
          'type' => 'number_integer',
          'weight' => 9,
      ])
      ->setDisplayOptions('form', [
          'type' => 'number',
          'weight' => 9,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE)
      ->setDefaultValue(0);

    // float | x  | X coordinate in pixels (double)
    $fields['x'] = BaseFieldDefinition::create('float')
      ->setLabel(t('X'))
      ->setDescription(t('X coordinate in pixels (double).'))
      ->setSetting('size', 'normal')
      ->setSetting('unsigned', TRUE)
      ->setDisplayOptions('view', [
          'label' => 'inline',
          'type' => 'number_integer',
          'weight' => 9,
      ])
      ->setDisplayOptions('form', [
          'type' => 'number',
          'weight' => 9,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE)
      ->setDefaultValue(0);

    // float | y  | Y coordinate in pixels (double)
    $fields['y'] = BaseFieldDefinition::create('float')
      ->setLabel(t('Y'))
      ->setDescription(t('Y coordinate in pixels (double).'))
      ->setSetting('size', 'normal')
      ->setSetting('unsigned', TRUE)
      ->setDisplayOptions('view', [
          'label' => 'inline',
          'type' => 'number_integer',
          'weight' => 10,
      ])
      ->setDisplayOptions('form', [
          'type' => 'number',
          'weight' => 10,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE)
      ->setDefaultValue(0);

    return $fields;
  }

  // int    | tmx_groupid | Id of a group of coordinates, equals to first coordinate id in the group.
  public function getTmxGroupid() {
    return $this->get('tmx_groupid')->value;
  }
  public function setTmxGroupid($value) {
    $this->set('tmx_groupid', $value);
    return $this;
  }

  // int    | crid | Row id
  public function getCrid() {
    return $this->get('crid')->value;
  }
  public function setCrid($value) {
    $this->set('crid', $value);
    return $this;
  }

  // float | x  | X coordinate in pixels (double)
  public function getX() {
    return $this->get('x')->value;
  }
  public function setX($value) {
    $this->set('x', $value);
    return $this;
  }

  // float | y  | Y coordinate in pixels (double)
  public function getY() {
    return $this->get('y')->value;
  }
  public function setY($value) {
    $this->set('y', $value);
    return $this;
  }

}
