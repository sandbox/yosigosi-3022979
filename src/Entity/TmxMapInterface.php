<?php

namespace Drupal\tmx\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;


/**
 * Provides an interface for defining TMX Map entities.
 *
 * @ingroup tmx
 */
interface TmxMapInterface extends ContentEntityInterface, EntityChangedInterface {

  // Add get/set methods for your configuration properties here.

  public function getName();
  public function setName($name);

  public function getOwner();
  public function getOwnerId();
  public function setOwnerId($uid);
  public function setOwner($account);
  
  public function getCreatedTime();
  public function setCreatedTime($timestamp);

  public function getChangedTime();
  public function setChangedTime($timestamp);
    
  public function isPublished();
  public function setPublished($published);
  
  public function getDefMapId();
  public function getDefMap();
  public function setDefMapId($entity_id);
  public function setDefMap(TmxDefMapInterface $object);

  public function isComplete();
  public function setComplete($value);
}
