<?php

namespace Drupal\tmx\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;

/**
 * Defines the TMX Wang Set Definition entity.
 *
 * @ingroup tmx
 *
 * @ContentEntityType(
 *   id = "tmx_def_wang_set",
 *   label = @Translation("TMX Wang Set Definition"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\tmx\TmxDefWangSetListBuilder",
 *     "views_data" = "Drupal\tmx\Entity\TmxDefWangSetViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\tmx\Form\TmxDefWangSetForm",
 *       "add" = "Drupal\tmx\Form\TmxDefWangSetForm",
 *       "edit" = "Drupal\tmx\Form\TmxDefWangSetForm",
 *       "delete" = "Drupal\tmx\Form\TmxDefWangSetDeleteForm",
 *     },
 *     "access" = "Drupal\tmx\TmxDefWangSetAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\tmx\TmxDefWangSetHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "tmx_def_wang_set",
 *   admin_permission = "administer tmx",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *   },
 *   links = {
 *     "canonical" = "/admin/content/tmx/tmx_def_wang_set/{tmx_def_wang_set}",
 *     "add-form" = "/admin/content/tmx/tmx_def_wang_set/add",
 *     "edit-form" = "/admin/content/tmx/tmx_def_wang_set/{tmx_def_wang_set}/edit",
 *     "delete-form" = "/admin/content/tmx/tmx_def_wang_set/{tmx_def_wang_set}/delete",
 *     "collection" = "/admin/content/tmx/def_wang_sets",
 *   },
 *   field_ui_base_route = "tmx_def_wang_set.settings"
 * )
 */
class TmxDefWangSet extends ContentEntityBase implements TmxDefWangSetInterface {

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

   // int      | tmx_wsid |  TMX Wang Set Id
   // string   | name |  Name of the Wang set
    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the entity.'))
      ->setSettings([
          'max_length' => 50,
          'text_processing' => 0,
      ])
      ->setDisplayOptions('view', [
          'label' => 'above',
          'type' => 'string',
          'weight' => 0,
      ])
      ->setDisplayOptions('form', [
          'type' => 'string_textfield',
          'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE)
      ->setDefaultValue('');
    
   // int      | tmx_def_tileset | Referenced Tileset
    $fields['tmx_def_tileset'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Tileset Definition'))
      ->setDescription(t('Referenced Tileset.'))
      ->setSetting('target_type', 'tmx_def_tileset')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('view', [
          'label' => 'hidden',
          'type' => 'entity_reference_entity_id',
          'weight' => -1,
      ])
      ->setDisplayOptions('form', [
          'type' => 'entity_reference_autocomplete',
          'settings' => [
              'match_operator' => 'CONTAINS',
              'size' => '60',
              'autocomplete_type' => 'tags',
              'placeholder' => '',
          ],
          'weight' => -1,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE)
      ->setDefaultValue(0);

   // int      | tmx_def_tileitem | Referenced Tileitem (Original: tile)
    $fields['tmx_def_tileitem'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Referenced Tileitem'))
      ->setDescription(t('Referenced Tileitem (Original: tile).'))
      ->setSetting('target_type', 'tmx_def_tileset')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('view', [
          'label' => 'hidden',
          'type' => 'entity_reference_entity_id',
          'weight' => -1,
      ])
      ->setDisplayOptions('form', [
          'type' => 'entity_reference_autocomplete',
          'settings' => [
              'match_operator' => 'CONTAINS',
              'size' => '60',
              'autocomplete_type' => 'tags',
              'placeholder' => '',
          ],
          'weight' => -1,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE)
      ->setDefaultValue(0);

    return $fields;
  }

  // int      | tmx_def_tileset | Referenced Tileset
  public function getDefTileset() {
    return $this->get('tmx_def_tileset')->entity;
  }
  public function getDefTilesetId() {
    return $this->get('tmx_def_tileset')->target_id;
  }
  public function setDefTilesetId($entity_id) {
    $this->set('tmx_def_tileset', $entity_id);
    return $this;
  }
  public function setDefTileset(Entity $entity) {
    $this->set('tmx_def_tileset', $entity->id());
    return $this;
  }

  // int      | tmx_def_tileitem | Referenced Tileitem (Original: tile)
  public function getDefTileitem() {
    return $this->get('tmx_def_tileitem')->entity;
  }
  public function getDefTileitemId() {
    return $this->get('tmx_def_tileitem')->target_id;
  }
  public function setDefTileitemId($entity_id) {
    $this->set('tmx_def_tileitem', $entity_id);
    return $this;
  }
  public function setDefTileitem(Entity $entity) {
    $this->set('tmx_def_tileitem', $entity->id());
    return $this;
  }

  // computed | cornercolors | Array of Wang colors (array).
  public function getCornerColors() {
    return []; // TBD
  }
  public function setCornerColors($collection) {
    // TBD
  }
  public function setCornerColor($value) {
    // TBD
  }

  // computed | edgecolors | Array of Wang colors (array).
  public function getEdgeColors() {
    return []; // TBD
  }
  public function setEdgeColors($collection) {
    // TBD
  }
  public function setEdgeColor($value) {
    // TBD
  }

  // computed | wangtiles | Array of Wang tiles (array).
  public function getWangTiles() {
    return []; // TBD
  }
  public function setWangTiles($collection) {
    // TBD
  }
  public function setWangTile($value) {
    // TBD
  }

}
