<?php

namespace Drupal\tmx\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;

/**
 * Defines the TMX Terrain Definition entity.
 *
 * @ingroup tmx
 *
 * @ContentEntityType(
 *   id = "tmx_def_terrain",
 *   label = @Translation("TMX Terrain Definition"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\tmx\TmxDefTerrainListBuilder",
 *     "views_data" = "Drupal\tmx\Entity\TmxDefTerrainViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\tmx\Form\TmxDefTerrainForm",
 *       "add" = "Drupal\tmx\Form\TmxDefTerrainForm",
 *       "edit" = "Drupal\tmx\Form\TmxDefTerrainForm",
 *       "delete" = "Drupal\tmx\Form\TmxDefTerrainDeleteForm",
 *     },
 *     "access" = "Drupal\tmx\TmxDefTerrainAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\tmx\TmxDefTerrainHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "tmx_def_terrain",
 *   admin_permission = "administer tmx",
 *   entity_keys = {
 *     "id" = "tmx_trid",
 *     "label" = "name",
 *   },
 *   links = {
 *     "canonical" = "/admin/content/tmx/tmx_def_terrain/{tmx_def_terrain}",
 *     "add-form" = "/admin/content/tmx/tmx_def_terrain/add",
 *     "edit-form" = "/admin/content/tmx/tmx_def_terrain/{tmx_def_terrain}/edit",
 *     "delete-form" = "/admin/content/tmx/tmx_def_terrain/{tmx_def_terrain}/delete",
 *     "collection" = "/admin/content/tmx/def_terrains",
 *   },
 *   field_ui_base_route = "tmx_def_terrain.settings"
 * )
 */
class TmxDefTerrain extends ContentEntityBase implements TmxDefTerrainInterface {

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    // int    | tmx_trid |  TMX Terrain Id
    // string | name  |  Name of terrain
    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the entity.'))
      ->setSettings([
          'max_length' => 50,
          'text_processing' => 0,
      ])
      ->setDisplayOptions('view', [
          'label' => 'above',
          'type' => 'string',
          'weight' => 0,
      ])
      ->setDisplayOptions('form', [
          'type' => 'string_textfield',
          'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE)
      ->setDefaultValue('');
    
    // int    | tmx_def_tileset | Referenced Tileset
    $fields['tmx_def_tileset'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Tileset Definition'))
      ->setDescription(t('Referenced Tileset.'))
      ->setSetting('target_type', 'tmx_def_tileset')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('view', [
          'label' => 'hidden',
          'type' => 'entity_reference_entity_id',
          'weight' => -1,
      ])
      ->setDisplayOptions('form', [
          'type' => 'entity_reference_autocomplete',
          'settings' => [
              'match_operator' => 'CONTAINS',
              'size' => '60',
              'autocomplete_type' => 'tags',
              'placeholder' => '',
          ],
          'weight' => -1,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE)
      ->setDefaultValue(0);

    // int    | tmx_def_tileitem | Referenced Tileitem (Original: tile)
    $fields['tmx_def_tileitem'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Referenced Tileitem'))
      ->setDescription(t('Referenced Tileitem (Original: tile).'))
      ->setSetting('target_type', 'tmx_def_tileset')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('view', [
          'label' => 'hidden',
          'type' => 'entity_reference_entity_id',
          'weight' => -1,
      ])
      ->setDisplayOptions('form', [
          'type' => 'entity_reference_autocomplete',
          'settings' => [
              'match_operator' => 'CONTAINS',
              'size' => '60',
              'autocomplete_type' => 'tags',
              'placeholder' => '',
          ],
          'weight' => -1,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE)
      ->setDefaultValue(0);

    return $fields;
  }

  // int    | tmx_def_tileset | Referenced Tileset
  public function getDefTileset() {
    return $this->get('tmx_def_tileset')->entity;
  }
  public function getDefTilesetId() {
    return $this->get('tmx_def_tileset')->target_id;
  }
  public function setDefTilesetId($entity_id) {
    $this->set('tmx_def_tileset', $entity_id);
    return $this;
  }
  public function setDefTileset(Entity $entity) {
    $this->set('tmx_def_tileset', $entity->id());
    return $this;
  }

  // int    | tmx_def_tileitem | Referenced Tileitem (Original: tile)
  public function getDefTileitem() {
    return $this->get('tmx_def_tileitem')->entity;
  }
  public function getDefTileitemId() {
    return $this->get('tmx_def_tileitem')->target_id;
  }
  public function setDefTileitemId($entity_id) {
    $this->set('tmx_def_tileitem', $entity_id);
    return $this;
  }
  public function setDefTileitem(Entity $entity) {
    $this->set('tmx_def_tileitem', $entity->id());
    return $this;
  }

}
