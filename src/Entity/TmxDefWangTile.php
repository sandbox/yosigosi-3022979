<?php

namespace Drupal\tmx\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;

/**
 * Defines the TMX Wang Tile Definition entity.
 *
 * @ingroup tmx
 *
 * @ContentEntityType(
 *   id = "tmx_def_wang_tile",
 *   label = @Translation("TMX Wang Tile Definition"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\tmx\TmxDefWangTileListBuilder",
 *     "views_data" = "Drupal\tmx\Entity\TmxDefWangTileViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\tmx\Form\TmxDefWangTileForm",
 *       "add" = "Drupal\tmx\Form\TmxDefWangTileForm",
 *       "edit" = "Drupal\tmx\Form\TmxDefWangTileForm",
 *       "delete" = "Drupal\tmx\Form\TmxDefWangTileDeleteForm",
 *     },
 *     "access" = "Drupal\tmx\TmxDefWangTileAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\tmx\TmxDefWangTileHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "tmx_def_wang_tile",
 *   admin_permission = "administer tmx",
 *   entity_keys = {
 *     "id" = "tmx_wtid",
 *   },
 *   links = {
 *     "canonical" = "/admin/content/tmx/tmx_def_wang_tile/{tmx_def_wang_tile}",
 *     "add-form" = "/admin/content/tmx/tmx_def_wang_tile/add",
 *     "edit-form" = "/admin/content/tmx/tmx_def_wang_tile/{tmx_def_wang_tile}/edit",
 *     "delete-form" = "/admin/content/tmx/tmx_def_wang_tile/{tmx_def_wang_tile}/delete",
 *     "collection" = "/admin/content/tmx/def_wang_tiles",
 *   },
 *   field_ui_base_route = "tmx_def_wang_tile.settings"
 * )
 */
class TmxDefWangTile extends ContentEntityBase implements TmxDefWangTileInterface { 

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    // int      | tmx_wtid |  TMX Wang Tile Id

    // int      | tmx_def_wang_set | Referenced Wang Set
    $fields['tmx_def_wang_set'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Referenced Wang Set'))
      ->setDescription(t('Referenced Wang Set.'))
      ->setSetting('target_type', 'tmx_def_wang_set')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('view', [
          'label' => 'hidden',
          'type' => 'entity_reference_entity_id',
          'weight' => -1,
      ])
      ->setDisplayOptions('form', [
          'type' => 'entity_reference_autocomplete',
          'settings' => [
              'match_operator' => 'CONTAINS',
              'size' => '60',
              'autocomplete_type' => 'tags',
              'placeholder' => '',
          ],
          'weight' => -1,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE)
      ->setDefaultValue(0);

    // int      | tmx_def_tileitem |  Local ID of tile (Original: tileid)
    $fields['tmx_def_tileitem'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Referenced Tileitem'))
      ->setDescription(t('Local ID of tile (Original: tileid).'))
      ->setSetting('target_type', 'tmx_def_tileset')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('view', [
          'label' => 'hidden',
          'type' => 'entity_reference_entity_id',
          'weight' => -1,
      ])
      ->setDisplayOptions('form', [
          'type' => 'entity_reference_autocomplete',
          'settings' => [
              'match_operator' => 'CONTAINS',
              'size' => '60',
              'autocomplete_type' => 'tags',
              'placeholder' => '',
          ],
          'weight' => -1,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE)
      ->setDefaultValue(0);

    // bool     | dflip  |  Tile is flipped diagonally
    $fields['dflip'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Flipped diagonally'))
      ->setDescription(t('Tile is flipped diagonally.'))
      ->setDisplayOptions('view', [
          'label' => 'inline',
          'type' => 'boolean',
          'weight' => 4,
      ])
      ->setDisplayOptions('form', [
          'type' => 'boolean_checkbox',
          'settings' => [
              'display_label' => TRUE,
          ],
          'weight' => 4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE)
      ->setDefaultValue(FALSE);

    // bool     | hflip  |  Tile is flipped horizontally
    $fields['hflip'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Flipped horizontally'))
      ->setDescription(t('Tile is flipped horizontally.'))
      ->setDisplayOptions('view', [
          'label' => 'inline',
          'type' => 'boolean',
          'weight' => 4,
      ])
      ->setDisplayOptions('form', [
          'type' => 'boolean_checkbox',
          'settings' => [
              'display_label' => TRUE,
          ],
          'weight' => 4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE)
      ->setDefaultValue(FALSE);

    // bool     | vflip  |  Tile is flipped vertically
    $fields['vflip'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Flipped vertically'))
      ->setDescription(t('Tile is flipped vertically.'))
      ->setDisplayOptions('view', [
          'label' => 'inline',
          'type' => 'boolean',
          'weight' => 4,
      ])
      ->setDisplayOptions('form', [
          'type' => 'boolean_checkbox',
          'settings' => [
              'display_label' => TRUE,
          ],
          'weight' => 4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE)
      ->setDefaultValue(TRUE);

    return $fields;
  }


  // int      | tmx_def_wang_set | Referenced Wang Set
  public function getDefWangSet() {
    return $this->get('tmx_def_wang_set')->entity;
  }
  public function getDefWangSetId() {
    return $this->get('tmx_def_wang_set')->target_id;
  }
  public function setDefWangSetId($entity_id) {
    $this->set('tmx_def_wang_set', $entity_id);
    return $this;
  }
  public function setDefWangSet(Entity $entity) {
    $this->set('tmx_def_wang_set', $entity->id());
    return $this;
  }

  // int      | tmx_def_tileitem |  Local ID of tile (Original: tileid)
  public function getDefTileitem() {
    return $this->get('tmx_def_tileitem')->entity;
  }
  public function getDefTileitemId() {
    return $this->get('tmx_def_tileitem')->target_id;
  }
  public function setDefTileitemId($entity_id) {
    $this->set('tmx_def_tileitem', $entity_id);
    return $this;
  }
  public function setDefTileitem(Entity $entity) {
    $this->set('tmx_def_tileitem', $entity->id());
    return $this;
  }

  // bool     | dflip  |  Tile is flipped diagonally
  public function isDFlip() {
    return (bool) $this->get('dflip')->value;
  }
  public function setDFlip($value) {
    $this->set('dflip', $value ? TRUE : FALSE);
    return $this;
  }

  // bool     | hflip  |  Tile is flipped horizontally
  public function isHFlip() {
    return (bool) $this->get('hflip')->value;
  }
  public function setHFlip($value) {
    $this->set('hflip', $value ? TRUE : FALSE);
    return $this;
  }

  // bool     | vflip  |  Tile is flipped vertically
  public function isVFlip() {
    return (bool) $this->get('vflip')->value;
  }
  public function setVFlip($value) {
    $this->set('vflip', $value ? TRUE : FALSE);
    return $this;
  }

  // computed | wangcolors | Wang color indexes (uchar[8]) (array) (Original: wangid)
  public function getWangColors() {
    return []; // TBD
  }
  public function setWangColors($collection) {
    // TBD
  }
  public function setWangColor($value) {
    // TBD
  }


}
