<?php

namespace Drupal\tmx\Entity;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Provides an interface for defining TMX Object Definition entities.
 *
 * @ingroup tmx
 */
interface TmxDefObjectInterface extends ContentEntityInterface {

  // Add get/set methods for your configuration properties here.

  // int    | tmx_obid |  TMX Object Id
  // computed | polygon  |  A list of x,y coordinates in pixels (array)
  // computed | polyline |  A list of x,y coordinates in pixels (array)
  // computed | properties |  A list of properties (name, value, type) (array)
  
  // int    | obid  |  Incremental id - unique across all objects
  public function getObid();
  public function setObid($value);
  
  // string | name  |  String assigned to name field in editor
  public function getName();
  public function setName($value);

  // string | type  |  String assigned to type field in editor
  public function getType();
  public function setType($value);
  
  // int    | gid  |  GID, only if object comes from a Tilemap
  public function getGid();
  public function setGid($value);
  
  // double | rotation  |  Angle in degrees clockwise
  public function getRotation();
  public function setRotation($value);

  // string | template  |  Reference to a template file, in case object is a template instance
  public function getTemplate();
  public function setTemplate($value);
    
  // text_long | text |  String key-value pairs (object)
  public function getText();
  public function setText($value);
  
  // bool   | ellipse  |  Used to mark an object as an ellipse
  public function isEllipse();
  public function setEllipse($value);
  
  // bool   | point  |  Used to mark an object as a point
  public function isPoint();
  public function setPoint($value);
  
  // bool   | visible  |  Whether object is shown in editor.
  public function isVisible();
  public function setVisible($value);

  // double | height  |  Height in pixels. Ignored if using a gid.
  public function geHeight();
  public function setHeight($value);
  
  // double | width  |  Width in pixels. Ignored if using a gid.
  public function getWidth();
  public function setWidth($value);
  
  // double | x  |  X coordinate in pixels
  public function getX();
  public function setX($value);
  
  // double | y  |  Y coordinate in pixels
  public function getY();
  public function setY($value);
  
  // computed | polygon  |  A list of x,y coordinates in pixels (array)
  public function getPolygon();
  
  // computed | polyline |  A list of x,y coordinates in pixels (array)
  public function getPolyline();
  
  // computed | properties |  A list of properties (name, value, type) (array)
  public function getProperties();
  
  
  
  
  
  
}
