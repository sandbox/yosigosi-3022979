<?php

namespace Drupal\tmx\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;

/**
 * Defines the TMX Map entity.
 *
 * @ingroup tmx
 *
 * @ContentEntityType(
 *   id = "tmx_map",
 *   label = @Translation("TMX Map"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\tmx\TmxMapListBuilder",
 *     "views_data" = "Drupal\tmx\Entity\TmxMapViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\tmx\Form\TmxMapForm",
 *       "add" = "Drupal\tmx\Form\TmxMapForm",
 *       "edit" = "Drupal\tmx\Form\TmxMapForm",
 *       "delete" = "Drupal\tmx\Form\TmxMapDeleteForm",
 *     },
 *     "access" = "Drupal\tmx\TmxMapAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\tmx\TmxMapHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "tmx_map",
 *   admin_permission = "administer tmx",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "status" = "status",
 *   },
 *   links = {
 *     "canonical" = "/admin/content/tmx/tmx_map/{tmx_map}",
 *     "add-form" = "/admin/content/tmx/tmx_map/add",
 *     "edit-form" = "/admin/content/tmx/tmx_map/{tmx_map}/edit",
 *     "delete-form" = "/admin/content/tmx/tmx_map/{tmx_map}/delete",
 *     "collection" = "/admin/content/tmx/maps",
 *   },
 *   field_ui_base_route = "tmx_map.settings"
 * )
 */
class TmxMap extends ContentEntityBase implements TmxMapInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   * @see: core/lib/Drupal/Core/Field/Plugin/Field
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);
    
    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the TMX Map entity.'))
      ->setSettings([
          'max_length' => 50,
          'text_processing' => 0,
      ])
      ->setDisplayOptions('view', [
          'label' => 'above',
          'type' => 'string',
          'weight' => 0,
      ])
      ->setDisplayOptions('form', [
          'type' => 'string_textfield',
          'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE)
      ->setDefaultValue('');
    
    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Owned by'))
      ->setDescription(t('The user ID owner of the TMX Map entity.'))
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE)
      ->setDefaultValue(0);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'))
      ->setDisplayOptions('view', [
          'label' => 'inline',
          'type' => 'timestamp',
          'weight' => 3,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE)
      ->setDefaultValue(0);

      $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'))
      ->setDisplayOptions('view', [
          'label' => 'inline',
          'type' => 'timestamp',
          'weight' => 4,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE)
      ->setDefaultValue(0);
      
      $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Publishing status'))
      ->setDescription(t('A boolean indicating whether the TMX Map is published.'))
      ->setDisplayOptions('view', [
          'label' => 'inline',
          'type' => 'boolean',
          'weight' => 0,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE)
      ->setDefaultValue(TRUE);

    $fields['complete'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('All Images dowloaded'))
      ->setDescription(t('A boolean indicating whether the map has all the images downloaded.'))
      ->setDefaultValue(FALSE)
      ->setDisplayOptions('view', [
          'label' => 'inline',
          'type' => 'boolean',
          'weight' => 6,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(FALSE)
      ->setDefaultValue(FALSE);

    $fields['tmx_def_map'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('TMX Definition Map'))
      ->setDescription(t('The ID of the TMX Definition Map entity.'))
      ->setSetting('target_type', 'tmx_def_map')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('view', [
          'label' => 'inline',
          'type' => 'entity_reference_label',
          'weight' => 5,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(FALSE)
      ->setDefaultValue(0);
      
    return $fields;
  }

  public function getName() {
    return $this->get('name')->value;
  }
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  public function getOwner() {
    return $this->get('user_id')->entity;
  }
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }
  public function setOwner($account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  public function getCreatedTime() {
    return $this->get('created')->value;
  }
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  public function getChangedTime() {
    return $this->get('changed')->value;
  }
  public function setChangedTime($timestamp) {
    $this->set('changed', $timestamp);
    return $this;
  }

  public function isPublished() {
    return (bool) $this->getEntityKey('status');
  }
  public function setPublished($published) {
    $this->set('status', $published ? TRUE : FALSE);
    return $this;
  }

  public function getDefMapId() {
    return $this->get('tmx_def_map')->target_id;
  }
  public function getDefMap() {
    return $this->get('tmx_def_map')->entity;
  }
  public function setDefMapId($entity_id) {
    $this->set('tmx_def_map', $entity_id);
    return $this;
  }
  public function setDefMap(TmxDefMapInterface $object) {
    $this->set('tmx_def_map', $object->id());
    return $this;
  }

  public function isComplete() {
    return (bool) $this->get('complete')->value;
  }
  public function setComplete($value) {
    $this->set('complete', $value ? TRUE : FALSE);
    return $this;
  }

}
