<?php

namespace Drupal\tmx\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;

/**
 * Defines the TMX Properties Definition entity.
 *
 * @ingroup tmx
 *
 * @ContentEntityType(
 *   id = "tmx_def_properties",
 *   label = @Translation("TMX Properties Definition"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\tmx\TmxDefPropertiesListBuilder",
 *     "views_data" = "Drupal\tmx\Entity\TmxDefPropertiesViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\tmx\Form\TmxDefPropertiesForm",
 *       "add" = "Drupal\tmx\Form\TmxDefPropertiesForm",
 *       "edit" = "Drupal\tmx\Form\TmxDefPropertiesForm",
 *       "delete" = "Drupal\tmx\Form\TmxDefPropertiesDeleteForm",
 *     },
 *     "access" = "Drupal\tmx\TmxDefPropertiesAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\tmx\TmxDefPropertiesHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "tmx_def_properties",
 *   admin_permission = "administer tmx",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *   },
 *   links = {
 *     "canonical" = "/admin/content/tmx/tmx_def_properties/{tmx_def_properties}",
 *     "add-form" = "/admin/content/tmx/tmx_def_properties/add",
 *     "edit-form" = "/admin/content/tmx/tmx_def_properties/{tmx_def_properties}/edit",
 *     "delete-form" = "/admin/content/tmx/tmx_def_properties/{tmx_def_properties}/delete",
 *     "collection" = "/admin/content/tmx/def_properties",
 *   },
 *   field_ui_base_route = "tmx_def_properties.settings"
 * )
 */
class TmxDefProperties extends ContentEntityBase implements TmxDefPropertiesInterface {

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    // int   | tmx_pid |  TMX Properties Id
    // string | name  | Property name
    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the entity.'))
      ->setSettings([
          'max_length' => 50,
          'text_processing' => 0,
      ])
      ->setDisplayOptions('view', [
          'label' => 'above',
          'type' => 'string',
          'weight' => 0,
      ])
      ->setDisplayOptions('form', [
          'type' => 'string_textfield',
          'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE)
      ->setDefaultValue('');
    
    // string | tmx_entity_type | Tipo de entidad referenciada. (map, layer, object, tileset, tile)
    $fields['tmx_entity_type'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Entity Type'))
      ->setDescription(t('Referenced Entity Type (map, layer, object, tileset, tile).'))
      ->setSettings([
          'max_length' => 50,
          'text_processing' => 0,
      ])
      ->setDisplayOptions('view', [
          'label' => 'above',
          'type' => 'string',
          'weight' => 3,
      ])
      ->setDisplayOptions('form', [
          'type' => 'string_textfield',
          'weight' => 3,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE)
      ->setDefaultValue('');

    // ref    | tmx_entity_id   | Referenced entity id
    $fields['tmx_entity_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Entity id'))
      ->setDescription(t('Referenced entity id.'))
//      ->setSetting('target_type', 'tmx_def_map', 'tmx_def_layer', 'tmx_def_object', 'tmx_def_tileset', 'tmx_def_tileitem', 'tmx_def_tile'])
      ->setSetting('handler', 'default')
      ->setDisplayOptions('view', [
          'label' => 'hidden',
          'type' => 'entity_reference_entity_id',
          'weight' => -1,
      ])
      ->setDisplayOptions('form', [
          'type' => 'entity_reference_autocomplete',
          'settings' => [
              'match_operator' => 'CONTAINS',
              'size' => '60',
              'autocomplete_type' => 'tags',
              'placeholder' => '',
          ],
          'weight' => -1,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE)
      ->setDefaultValue(0);

    // string | type  | Property type
    $fields['type'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Type'))
      ->setDescription(t('Property type.'))
      ->setSettings([
          'max_length' => 50,
          'text_processing' => 0,
      ])
      ->setDisplayOptions('view', [
          'label' => 'above',
          'type' => 'string',
          'weight' => 3,
      ])
      ->setDisplayOptions('form', [
          'type' => 'string_textfield',
          'weight' => 3,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE)
      ->setDefaultValue('');

    // string | value | Property value
    $fields['value'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Value'))
      ->setDescription(t('Property value.'))
      ->setSettings([
          'max_length' => 50,
          'text_processing' => 0,
      ])
      ->setDisplayOptions('view', [
          'label' => 'above',
          'type' => 'string',
          'weight' => 3,
      ])
      ->setDisplayOptions('form', [
          'type' => 'string_textfield',
          'weight' => 3,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE)
      ->setDefaultValue('');

    return $fields;
  }

  // string | tmx_entity_type | Tipo de entidad referenciada. (map, layer, object, tileset, tile)
  public function getEntityType() {
    return $this->get('tmx_entity_type')->value;
  }
  public function setEntityType($value) {
    $this->set('tmx_entity_type', $value);
    return $this;
  }

  // ref    | tmx_entity_id   | Identidad de la entidad referenciada
  public function getEntityId() {
    return $this->get('tmx_entity_id')->entity;
  }
  public function getEntityIdId() {
    return $this->get('tmx_entity_id')->target_id;
  }
  public function setEntityIdId($entity_id) {
    $this->set('tmx_entity_id', $entity_id);
    return $this;
  }
  public function setEntityId(Entity $entity) {
    $this->set('tmx_entity_id', $entity->id());
    return $this;
  }

  // string | type  | Property type
  public function getType() {
    return $this->get('type')->value;
  }
  public function setType($value) {
    $this->set('type', $value);
    return $this;
  }

  // string | value | Property value
  public function getValue() {
    return $this->get('value')->value;
  }
  public function setValue($value) {
    $this->set('value', $value);
    return $this;
  }

}
