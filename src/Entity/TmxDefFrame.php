<?php

namespace Drupal\tmx\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;

/**
 * Defines the TMX Frame Definition entity.
 *
 * @ingroup tmx
 *
 * @ContentEntityType(
 *   id = "tmx_def_frame",
 *   label = @Translation("TMX Frame Definition"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\tmx\TmxDefFrameListBuilder",
 *     "views_data" = "Drupal\tmx\Entity\TmxDefFrameViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\tmx\Form\TmxDefFrameForm",
 *       "add" = "Drupal\tmx\Form\TmxDefFrameForm",
 *       "edit" = "Drupal\tmx\Form\TmxDefFrameForm",
 *       "delete" = "Drupal\tmx\Form\TmxDefFrameDeleteForm",
 *     },
 *     "access" = "Drupal\tmx\TmxDefFrameAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\tmx\TmxDefFrameHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "tmx_def_frame",
 *   admin_permission = "administer tmx",
 *   entity_keys = {
 *     "id" = "tmx_fid",
 *   },
 *   links = {
 *     "canonical" = "/admin/content/tmx/tmx_def_frame/{tmx_def_frame}",
 *     "add-form" = "/admin/content/tmx/tmx_def_frame/add",
 *     "edit-form" = "/admin/content/tmx/tmx_def_frame/{tmx_def_frame}/edit",
 *     "delete-form" = "/admin/content/tmx/tmx_def_frame/{tmx_def_frame}/delete",
 *     "collection" = "/admin/content/tmx/def_frames",
 *   },
 *   field_ui_base_route = "tmx_def_frame.settings"
 * )
 */
class TmxDefFrame extends ContentEntityBase implements TmxDefFrameInterface {

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

   // int  | tmx_def_tileitem_group | Referenced Tileitem
    $fields['tmx_def_tileitem_group'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Referenced Tileitem Group'))
      ->setDescription(t('Referenced Tileitem.'))
      ->setSetting('target_type', 'tmx_def_tileitem')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('view', [
          'label' => 'hidden',
          'type' => 'entity_reference_entity_id',
          'weight' => -1,
      ])
      ->setDisplayOptions('form', [
          'type' => 'entity_reference_autocomplete',
          'settings' => [
              'match_operator' => 'CONTAINS',
              'size' => '60',
              'autocomplete_type' => 'tags',
              'placeholder' => '',
          ],
          'weight' => -1,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE)
      ->setDefaultValue(0);

   // int  | tmx_def_tileitem_frame  |  Tileitem reference representing this frame (Original: tileid)
    $fields['tmx_def_tileitem_frame'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Referenced Tileitem Element'))
      ->setDescription(t('Tileitem reference representing this frame (Original: tileid).'))
      ->setSetting('target_type', 'tmx_def_tileitem')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('view', [
          'label' => 'hidden',
          'type' => 'entity_reference_entity_id',
          'weight' => -1,
      ])
      ->setDisplayOptions('form', [
          'type' => 'entity_reference_autocomplete',
          'settings' => [
              'match_operator' => 'CONTAINS',
              'size' => '60',
              'autocomplete_type' => 'tags',
              'placeholder' => '',
          ],
          'weight' => -1,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE)
      ->setDefaultValue(0);

    // int  | duration  |  Frame duration in milliseconds
    $fields['duration'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Duration'))
      ->setDescription(t('Frame duration in milliseconds.'))
      ->setSetting('size', 'normal')
      ->setSetting('unsigned', TRUE)
      ->setDisplayOptions('view', [
          'label' => 'inline',
          'type' => 'number_integer',
          'weight' => 5,
      ])
      ->setDisplayOptions('form', [
          'type' => 'number',
          'weight' => 5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE)
      ->setDefaultValue(0);
    return $fields;
  }

  // int  | tmx_def_tileitem_group | Referenced Tileitem
  public function getDefTileitemGroup() {
    return $this->get('tmx_def_tileitem_group')->entity;
  }
  public function getDefTileitemGroupId() {
    return $this->get('tmx_def_tileitem_group')->target_id;
  }
  public function setDefTileitemGroupId($entity_id) {
    $this->set('tmx_def_tileitem_group', $entity_id);
    return $this;
  }
  public function setDefTileitemGroup(Entity $entity) {
    $this->set('tmx_def_tileitem_group', $entity->id());
    return $this;
  }

 // int  | tmx_def_tileitem_frame |  Tileitem reference representing this frame (Original: tileid)
  public function getDefTileitemFrame() {
    return $this->get('tmx_def_tileitem_frame')->entity;
  }
  public function getDefTileitemFrameId() {
    return $this->get('tmx_def_tileitem_frame')->target_id;
  }
  public function setDefTileitemFrameId($entity_id) {
    $this->set('tmx_def_tileitem_frame', $entity_id);
    return $this;
  }
  public function setDefTileitemFrame(Entity $entity) {
    $this->set('tmx_def_tileitem_frame', $entity->id());
    return $this;
  }

  // int  | duration  |  Frame duration in milliseconds
  public function getDuration() {
    return $this->get('duration')->value;
  }
  public function setDuration($value) {
    $this->set('duration', $value);
    return $this;
  }

}
