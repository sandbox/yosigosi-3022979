<?php

namespace Drupal\tmx\Entity;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Provides an interface for defining TMX Frame Definition entities.
 *
 * @ingroup tmx
 */
interface TmxDefFrameInterface extends ContentEntityInterface {

  // Add get/set methods for your configuration properties here.

  // int  | tmx_def_tileitem_group | Referenced Tileitem
  public function getDefTileitemGroup();
  public function getDefTileitemGroupId();
  public function setDefTileitemGroupId($entity_id);
  public function setDefTileitemGroup(Entity $entity);

 // int  | tmx_def_tileitem_frame |  Tileitem reference representing this frame (Original: tileid)
  public function getDefTileitemFrame();
  public function getDefTileitemFrameId();
  public function setDefTileitemFrameId($entity_id);
  public function setDefTileitemFrame(Entity $entity);

  // int  | duration  |  Frame duration in milliseconds
  public function getDuration();
  public function setDuration($value);

}
