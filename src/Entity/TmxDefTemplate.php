<?php

namespace Drupal\tmx\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;

/**
 * Defines the TMX Template Definition entity.
 *
 * @ingroup tmx
 *
 * @ContentEntityType(
 *   id = "tmx_def_template",
 *   label = @Translation("TMX Template Definition"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\tmx\TmxDefTemplateListBuilder",
 *     "views_data" = "Drupal\tmx\Entity\TmxDefTemplateViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\tmx\Form\TmxDefTemplateForm",
 *       "add" = "Drupal\tmx\Form\TmxDefTemplateForm",
 *       "edit" = "Drupal\tmx\Form\TmxDefTemplateForm",
 *       "delete" = "Drupal\tmx\Form\TmxDefTemplateDeleteForm",
 *     },
 *     "access" = "Drupal\tmx\TmxDefTemplateAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\tmx\TmxDefTemplateHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "tmx_def_template",
 *   admin_permission = "administer tmx",
 *   entity_keys = {
 *     "id" = "tmx_tmid",
 *   },
 *   links = {
 *     "canonical" = "/admin/content/tmx/tmx_def_template/{tmx_def_template}",
 *     "add-form" = "/admin/content/tmx/tmx_def_template/add",
 *     "edit-form" = "/admin/content/tmx/tmx_def_template/{tmx_def_template}/edit",
 *     "delete-form" = "/admin/content/tmx/tmx_def_template/{tmx_def_template}/delete",
 *     "collection" = "/admin/content/tmx/def_templates",
 *   },
 *   field_ui_base_route = "tmx_def_template.settings"
 * )
 */
class TmxDefTemplate extends ContentEntityBase implements TmxDefTemplateInterface {

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    // int     | tmx_otid |  TMX Object Template Id

    //  ref     | tmx_def_object | The object instantiated by this template (ref) (ref Object, original name: object)
    $fields['tmx_def_object'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Object Definition'))
      ->setDescription(t('The object instantiated by this template (ref) (ref Object, original name: object).'))
      ->setSetting('target_type', 'tmx_def_map')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('view', [
          'label' => 'hidden',
          'type' => 'entity_reference_entity_id',
          'weight' => -1,
      ])
      ->setDisplayOptions('form', [
          'type' => 'entity_reference_autocomplete',
          'settings' => [
              'match_operator' => 'CONTAINS',
              'size' => '60',
              'autocomplete_type' => 'tags',
              'placeholder' => '',
          ],
          'weight' => -1,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE)
      ->setDefaultValue(0);

    //  string  | type  | template file name
    $fields['type'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Type'))
      ->setDescription(t('template file name.'))
      ->setSettings([
          'max_length' => 50,
          'text_processing' => 0,
      ])
      ->setDisplayOptions('view', [
          'label' => 'above',
          'type' => 'string',
          'weight' => 3,
      ])
      ->setDisplayOptions('form', [
          'type' => 'string_textfield',
          'weight' => 3,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE)
      ->setDefaultValue('');


    return $fields;
  }

  //  ref     | tmx_def_object | The object instantiated by this template (ref) (ref Object, original name: object)
  public function getDefObject() {
    return $this->get('tmx_def_object')->entity;
  }
  public function getDefObjectId() {
    return $this->get('tmx_def_object')->target_id;
  }
  public function setDefObjectId($entity_id) {
    $this->set('tmx_def_object', $entity_id);
    return $this;
  }
  public function setDefObject(Entity $entity) {
    $this->set('tmx_def_object', $entity->id());
    return $this;
  }


  //  string  | type  | template file name
  public function getType() {
    return $this->get('type')->value;
  }
  public function setType($value) {
    $this->set('type', $value);
    return $this;
  }

  //  computed | tileset | External tileset used by the template (optional) (ref Tileset)
  public function getTileset() {
    return NULL;
  }
  public function setTileset($value) {
    // TBD
  }

}
