<?php

namespace Drupal\tmx\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;

/**
 * Defines the TMX Tileset Definition entity.
 *
 * @ingroup tmx
 *
 * @ContentEntityType(
 *   id = "tmx_def_tileset",
 *   label = @Translation("TMX Tileset Definition"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\tmx\TmxDefTilesetListBuilder",
 *     "views_data" = "Drupal\tmx\Entity\TmxDefTilesetViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\tmx\Form\TmxDefTilesetForm",
 *       "add" = "Drupal\tmx\Form\TmxDefTilesetForm",
 *       "edit" = "Drupal\tmx\Form\TmxDefTilesetForm",
 *       "delete" = "Drupal\tmx\Form\TmxDefTilesetDeleteForm",
 *     },
 *     "access" = "Drupal\tmx\TmxDefTilesetAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\tmx\TmxDefTilesetHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "tmx_def_tileset",
 *   admin_permission = "administer tmx",
 *   entity_keys = {
 *     "id" = "tmx_tsid",
 *     "label" = "name",
 *   },
 *   links = {
 *     "canonical" = "/admin/content/tmx/tmx_def_tileset/{tmx_def_tileset}",
 *     "add-form" = "/admin/content/tmx/tmx_def_tileset/add",
 *     "edit-form" = "/admin/content/tmx/tmx_def_tileset/{tmx_def_tileset}/edit",
 *     "delete-form" = "/admin/content/tmx/tmx_def_tileset/{tmx_def_tileset}/delete",
 *     "collection" = "/admin/content/tmx/def_tilesets",
 *   },
 *   field_ui_base_route = "tmx_def_tileset.settings"
 * )
 */
class TmxDefTileset extends ContentEntityBase implements TmxDefTilesetInterface {

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    // int | tmx_tsid | TMX Tileset Id
    
    // string   | name  |  Name given to this tileset
    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the entity.'))
      ->setSettings([
          'max_length' => 50,
          'text_processing' => 0,
      ])
      ->setDisplayOptions('view', [
          'label' => 'above',
          'type' => 'string',
          'weight' => 0,
      ])
      ->setDisplayOptions('form', [
          'type' => 'string_textfield',
          'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE)
      ->setDefaultValue('');
    
    // ref      | tmx_def_map | Referended Map
    $fields['tmx_def_map'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Map Definition'))
      ->setDescription(t('Referended Map.'))
      ->setSetting('target_type', 'tmx_def_map')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('view', [
          'label' => 'hidden',
          'type' => 'entity_reference_entity_id',
          'weight' => -1,
      ])
      ->setDisplayOptions('form', [
          'type' => 'entity_reference_autocomplete',
          'settings' => [
              'match_operator' => 'CONTAINS',
              'size' => '60',
              'autocomplete_type' => 'tags',
              'placeholder' => '',
          ],
          'weight' => -1,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE)
      ->setDefaultValue(0);

    // ref      | tmx_def_template | Referended Template
    $fields['tmx_def_template'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Object Definition'))
      ->setDescription(t('Referended Template.'))
      ->setSetting('target_type', 'tmx_def_template')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('view', [
          'label' => 'hidden',
          'type' => 'entity_reference_entity_id',
          'weight' => -1,
      ])
      ->setDisplayOptions('form', [
          'type' => 'entity_reference_autocomplete',
          'settings' => [
              'match_operator' => 'CONTAINS',
              'size' => '60',
              'autocomplete_type' => 'tags',
              'placeholder' => '',
          ],
          'weight' => -1,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE)
      ->setDefaultValue(0);

    // string   | type  |  tileset (for tileset files, since 1.0)
    $fields['type'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Type'))
      ->setDescription(t('tileset (for tileset files, since 1.0).'))
      ->setSettings([
          'max_length' => 50,
          'text_processing' => 0,
      ])
      ->setDisplayOptions('view', [
          'label' => 'above',
          'type' => 'string',
          'weight' => 3,
      ])
      ->setDisplayOptions('form', [
          'type' => 'string_textfield',
          'weight' => 3,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE)
      ->setDefaultValue('tileset');

    // int      | columns  |  The number of tile columns in the tileset
    $fields['columns'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Columns'))
      ->setDescription(t('The number of tile columns in the tileset.'))
      ->setSetting('size', 'normal')
      ->setSetting('unsigned', TRUE)
      ->setDisplayOptions('view', [
          'label' => 'inline',
          'type' => 'number_integer',
          'weight' => 5,
      ])
      ->setDisplayOptions('form', [
          'type' => 'number',
          'weight' => 5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE)
      ->setDefaultValue(0);

    // int      | margin  |  Buffer between image edge and first tile (pixels)
    $fields['margin'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Margin'))
      ->setDescription(t('Buffer between image edge and first tile (pixels).'))
      ->setSetting('size', 'normal')
      ->setSetting('unsigned', TRUE)
      ->setDisplayOptions('view', [
          'label' => 'inline',
          'type' => 'number_integer',
          'weight' => 5,
      ])
      ->setDisplayOptions('form', [
          'type' => 'number',
          'weight' => 5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE)
      ->setDefaultValue(0);

    // int      | spacing  |  Spacing between adjacent tiles in image (pixels)
    $fields['spacing'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Spacing'))
      ->setDescription(t('Spacing between adjacent tiles in image (pixels).'))
      ->setSetting('size', 'normal')
      ->setSetting('unsigned', TRUE)
      ->setDisplayOptions('view', [
          'label' => 'inline',
          'type' => 'number_integer',
          'weight' => 5,
      ])
      ->setDisplayOptions('form', [
          'type' => 'number',
          'weight' => 5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE)
      ->setDefaultValue(0);

    // int      | tilecount  |  The number of tiles in this tileset
    $fields['tilecount'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Tile count'))
      ->setDescription(t('The number of tiles in this tileset.'))
      ->setSetting('size', 'normal')
      ->setSetting('unsigned', TRUE)
      ->setDisplayOptions('view', [
          'label' => 'inline',
          'type' => 'number_integer',
          'weight' => 5,
      ])
      ->setDisplayOptions('form', [
          'type' => 'number',
          'weight' => 5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE)
      ->setDefaultValue(0);

    // int      | tileheight  |  Maximum height of tiles in this set
    $fields['tileheight'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Tile height'))
      ->setDescription(t('Maximum height of tiles in this set.'))
      ->setSetting('size', 'normal')
      ->setSetting('unsigned', TRUE)
      ->setDisplayOptions('view', [
          'label' => 'inline',
          'type' => 'number_integer',
          'weight' => 5,
      ])
      ->setDisplayOptions('form', [
          'type' => 'number',
          'weight' => 5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE)
      ->setDefaultValue(0);

    // int      | tilewidth  |  Maximum width of tiles in this set
    $fields['tilewidth'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Tile width'))
      ->setDescription(t('Maximum width of tiles in this set.'))
      ->setSetting('size', 'normal')
      ->setSetting('unsigned', TRUE)
      ->setDisplayOptions('view', [
          'label' => 'inline',
          'type' => 'number_integer',
          'weight' => 5,
      ])
      ->setDisplayOptions('form', [
          'type' => 'number',
          'weight' => 5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE)
      ->setDefaultValue(0);

    // int      | firstgid  |  GID corresponding to the first tile in the set
    $fields['firstgid'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('First GID'))
      ->setDescription(t('GID corresponding to the first tile in the set.'))
      ->setSetting('size', 'normal')
      ->setSetting('unsigned', TRUE)
      ->setDisplayOptions('view', [
          'label' => 'inline',
          'type' => 'number_integer',
          'weight' => 5,
      ])
      ->setDisplayOptions('form', [
          'type' => 'number',
          'weight' => 5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE)
      ->setDefaultValue(0);

    // string   | image  |  Image used for tiles in this set
    $fields['image'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Image'))
      ->setDescription(t('Image used for tiles in this set.'))
      ->setSettings([
          'max_length' => 50,
          'text_processing' => 0,
      ])
      ->setDisplayOptions('view', [
          'label' => 'above',
          'type' => 'string',
          'weight' => 11,
      ])
      ->setDisplayOptions('form', [
          'type' => 'string_textfield',
          'weight' => 11,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(FALSE)
      ->setDefaultValue('');

    // ref       | tmx_img_fid |  Image used by this layer. imagelayer only.
    $fields['tmx_img_fid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Image file id'))
      ->setDescription(t('The Image file id that implements the field image.'))
      ->setSetting('target_type', 'file')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('view', [
          'label' => 'hidden',
          'type' => 'entity_reference_label',
          'weight' => 12,
      ])
      ->setDisplayOptions('form', [
          'type' => 'entity_reference_autocomplete',
          'settings' => [
              'match_operator' => 'CONTAINS',
              'size' => '60',
              'autocomplete_type' => 'tags',
              'placeholder' => '',
          ],
          'weight' => 12,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE)
      ->setDefaultValue(0);

    // int      | imagewidth  |  Width of source image in pixels
    $fields['imagewidth'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Image width'))
      ->setDescription(t('Width of source image in pixels.'))
      ->setSetting('size', 'normal')
      ->setSetting('unsigned', TRUE)
      ->setDisplayOptions('view', [
          'label' => 'inline',
          'type' => 'number_integer',
          'weight' => 5,
      ])
      ->setDisplayOptions('form', [
          'type' => 'number',
          'weight' => 5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE)
      ->setDefaultValue(0);

    // int      | imageheight  |  Height of source image in pixels
    $fields['imageheight'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Image height'))
      ->setDescription(t('Height of source image in pixels.'))
      ->setSetting('size', 'normal')
      ->setSetting('unsigned', TRUE)
      ->setDisplayOptions('view', [
          'label' => 'inline',
          'type' => 'number_integer',
          'weight' => 5,
      ])
      ->setDisplayOptions('form', [
          'type' => 'number',
          'weight' => 5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE)
      ->setDefaultValue(0);

    // string   | transparentcolor  |  Hex-formatted color (#RRGGBB) (optional)
    $fields['transparentcolor'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Transparent color'))
      ->setDescription(t('Hex-formatted color (#RRGGBB) (optional).'))
      ->setSettings([
          'max_length' => 50,
          'text_processing' => 0,
      ])
      ->setDisplayOptions('view', [
          'label' => 'above',
          'type' => 'string',
          'weight' => 2,
      ])
      ->setDisplayOptions('form', [
          'type' => 'string_textfield',
          'weight' => 2,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE)
      ->setDefaultValue('');

    // string   | gridtype  | Grid. Orientation of the grid for the tiles in this tileset (orthogonal or isometric). only isometric (optional)
    $fields['gridtype'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Grid type'))
      ->setDescription(t('Grid. Orientation of the grid for the tiles in this tileset (orthogonal or isometric). only isometric (optional).'))
      ->setSettings([
          'max_length' => 50,
          'text_processing' => 0,
      ])
      ->setDisplayOptions('view', [
          'label' => 'above',
          'type' => 'string',
          'weight' => 2,
      ])
      ->setDisplayOptions('form', [
          'type' => 'string_textfield',
          'weight' => 2,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE)
      ->setDefaultValue('');


    // int      | gridwidth | Grid. Width of a grid cell (optional)
    $fields['gridwidth'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Grid width'))
      ->setDescription(t('Grid. Width of a grid cell (optional).'))
      ->setSetting('size', 'normal')
      ->setSetting('unsigned', TRUE)
      ->setDisplayOptions('view', [
          'label' => 'inline',
          'type' => 'number_integer',
          'weight' => 5,
      ])
      ->setDisplayOptions('form', [
          'type' => 'number',
          'weight' => 5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE)
      ->setDefaultValue(0);

    // int      | gridheight | Grid. Height of a grid cell (optional)
    $fields['gridheight'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Grid height'))
      ->setDescription(t('Grid. Height of a grid cell (optional)'))
      ->setSetting('size', 'normal')
      ->setSetting('unsigned', TRUE)
      ->setDisplayOptions('view', [
          'label' => 'inline',
          'type' => 'number_integer',
          'weight' => 5,
      ])
      ->setDisplayOptions('form', [
          'type' => 'number',
          'weight' => 5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE)
      ->setDefaultValue(0);

    // int      | tileoffsetx | Tileoffset. Horizontal offset in pixels (optional).
    $fields['tileoffsetx'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Tile offset x'))
      ->setDescription(t('Tileoffset. Horizontal offset in pixels (optional).'))
      ->setSetting('size', 'normal')
      ->setSetting('unsigned', TRUE)
      ->setDisplayOptions('view', [
          'label' => 'inline',
          'type' => 'number_integer',
          'weight' => 5,
      ])
      ->setDisplayOptions('form', [
          'type' => 'number',
          'weight' => 5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE)
      ->setDefaultValue(0);

    // int      | tileoffsety | Tileoffset. Vertical offset in pixels (positive is down) (optional).
    $fields['tileoffsety'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Tile offset y'))
      ->setDescription(t('Tileoffset. Vertical offset in pixels (positive is down) (optional).'))
      ->setSetting('size', 'normal')
      ->setSetting('unsigned', TRUE)
      ->setDisplayOptions('view', [
          'label' => 'inline',
          'type' => 'number_integer',
          'weight' => 5,
      ])
      ->setDisplayOptions('form', [
          'type' => 'number',
          'weight' => 5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE)
      ->setDefaultValue(0);

    return $fields;
  }

  // ref      | tmx_def_map | Referended Map
  public function getDefMap() {
    return $this->get('tmx_def_map')->entity;
  }
  public function getDefMapId() {
    return $this->get('tmx_def_map')->target_id;
  }
  public function setDefMapId($entity_id) {
    $this->set('tmx_def_map', $entity_id);
    return $this;
  }
  public function setDefMap(Entity $entity) {
    $this->set('tmx_def_map', $entity->id());
    return $this;
  }

  // ref      | tmx_def_template | Referended Template
  public function getDefTemplate() {
    return $this->get('tmx_def_template')->entity;
  }
  public function getDefTemplateId() {
    return $this->get('tmx_def_template')->target_id;
  }
  public function setDefTemplateId($entity_id) {
    $this->set('tmx_def_template', $entity_id);
    return $this;
  }
  public function setDefTemplate(Entity $entity) {
    $this->set('tmx_def_template', $entity->id());
    return $this;
  }

  // string   | name  |  Name given to this tileset
  public function getName() {
    return $this->get('name')->value;
  }
  public function setName($value) {
    $this->set('name', $value);
    return $this;
  }

  // string   | type  |  tileset (for tileset files, since 1.0)
  public function getType() {
    return $this->get('type')->value;
  }
  public function setType($value) {
    $this->set('type', $value);
    return $this;
  }

  // int      | columns  |  The number of tile columns in the tileset
  public function getColumns() {
    return $this->get('columns')->value;
  }
  public function setColumns($value) {
    $this->set('columns', $value);
    return $this;
  }

  // int      | margin  |  Buffer between image edge and first tile (pixels)
  public function getMargin() {
    return $this->get('margin')->value;
  }
  public function setMargin($value) {
    $this->set('margin', $value);
    return $this;
  }

  // int      | spacing  |  Spacing between adjacent tiles in image (pixels)
  public function getSpacing() {
    return $this->get('spacing')->value;
  }
  public function setSpacing($value) {
    $this->set('spacing', $value);
    return $this;
  }

  // int      | tilecount  |  The number of tiles in this tileset
  public function getTileCount() {
    return $this->get('tilecount')->value;
  }
  public function setTileCount($value) {
    $this->set('tilecount', $value);
    return $this;
  }

  // int      | tileheight  |  Maximum height of tiles in this set
  public function getTileHeight() {
    return $this->get('tileheight')->value;
  }
  public function setTileHeight($value) {
    $this->set('tileheight', $value);
    return $this;
  }

  // int      | tilewidth  |  Maximum width of tiles in this set
  public function getTileWidth() {
    return $this->get('tilewidth')->value;
  }
  public function setTileWidth($value) {
    $this->set('tilewidth', $value);
    return $this;
  }

  // int      | firstgid  |  GID corresponding to the first tile in the set
  public function getFirstGid() {
    return $this->get('firstgid')->value;
  }
  public function setFirstGid($value) {
    $this->set('firstgid', $value);
    return $this;
  }

  // string   | image  |  Image used for tiles in this set
  public function getImage() {
    return $this->get('image')->value;
  }
  public function setImage($value) {
    $this->set('image', $value);
    return $this;
  }

  // ref       | tmx_img_fid |  Image used by this layer. imagelayer only.
  public function getImageFile() {
    return $this->get('tmx_img_fid')->entity;
  }
  public function getImageFileId() {
    return $this->get('tmx_img_fid')->target_id;
  }
  public function setImageFileId($entity_id) {
    $this->set('tmx_img_fid', $entity_id);
    return $this;
  }
  public function setImageFile(Entity $entity) {
    $this->set('tmx_img_fid', $entity->id());
    return $this;
  }

  // int      | imagewidth  |  Width of source image in pixels
  public function getImageWidth() {
    return $this->get('imagewidth')->value;
  }
  public function setImageWidth($value) {
    $this->set('imagewidth', $value);
    return $this;
  }

  // int      | imageheight  |  Height of source image in pixels
  public function getImageHeight() {
    return $this->get('imageheight')->value;
  }
  public function setImageHeight($value) {
    $this->set('imageheight', $value);
    return $this;
  }

  // string   | transparentcolor  |  Hex-formatted color (#RRGGBB) (optional)
  public function getTransparentColor() {
    return $this->get('transparentcolor')->value;
  }
  public function setTransparentColor($value) {
    $this->set('transparentcolor', $value);
    return $this;
  }

  // string   | gridtype  | Grid. Orientation of the grid for the tiles in this tileset (orthogonal or isometric). only isometric (optional)
  public function getGridType() {
    return $this->get('gridtype')->value;
  }
  public function setGridType($value) {
    $this->set('gridtype', $value);
    return $this;
  }

  // int      | gridwidth | Grid. Width of a grid cell (optional)
  public function getGridWidth() {
    return $this->get('gridwidth')->value;
  }
  public function setGridWidth($value) {
    $this->set('gridwidth', $value);
    return $this;
  }

  // int      | gridheight | Grid. Height of a grid cell (optional)
  public function getGridHeight() {
    return $this->get('gridheight')->value;
  }
  public function setGridHeight($value) {
    $this->set('gridheight', $value);
    return $this;
  }

  // int      | tileoffsetx | Tileoffset. Horizontal offset in pixels (optional).
  public function getTileOffsetX() {
    return $this->get('tileoffsetx')->value;
  }
  public function setTileOffsetX($value) {
    $this->set('tileoffsetx', $value);
    return $this;
  }

  // int      | tileoffsety | Tileoffset. Vertical offset in pixels (positive is down) (optional).
  public function getTileOffsetY() {
    return $this->get('tileoffsety')->value;
  }
  public function setTileOffsetY($value) {
    $this->set('tileoffsety', $value);
    return $this;
  }

  // computed | properties |  A list of properties (name, value, type) (array).
  public function getProperties() {
    return []; // TBD
  }
  public function setProperties($collection) {
    // TBD
  }
  public function setProperty($value) {
    // TBD
  }

  // computed | terrains |  Array of Terrains (optional) (array).
  public function getTerrains() {
    return []; // TBD
  }
  public function setTerrains($collection) {
    // TBD
  }
  public function setTerrain($value) {
    // TBD
  }

  // computed | tiles | Array of Tiles (optional) (array).
  public function getTiles() {
    return []; // TBD
  }
  public function setTiles($collection) {
    // TBD
  }
  public function setTile($value) {
    // TBD
  }

  // computed | wangsets | Array of Wang sets (since 1.1.5) (array).
  public function getWangsets() {
    return []; // TBD
  }
  public function setWangsets($collection) {
    // TBD
  }
  public function setWangset($value) {
    // TBD
  }

  // computed | grid | Grid. (optional) Build from fields: gridwidth(width), gridheight(height), gridtype(orientation)
  public function getGrid() {
    return []; // TBD
  }
  public function setGrid($value) {
    // TBD
  }

  // computed | tileoffset | Tileoffset. (optional) (object) Built from: tileoffsetx(x), tileoffsety(y)
  public function getTileoffset() {
    return []; // TBD
  }
  public function setTileoffset($value) {
    // TBD
  }

}
