<?php

namespace Drupal\tmx\Entity;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Provides an interface for defining TMX Properties Definition entities.
 *
 * @ingroup tmx
 */
interface TmxDefPropertiesInterface extends ContentEntityInterface {

  // Add get/set methods for your configuration properties here.

  // string | tmx_entity_type | Tipo de entidad referenciada. (map, layer, object, tileset, tile)
  public function getEntityType();
  public function setEntityType($value);

  // ref    | tmx_entity_id   | Identidad de la entidad referenciada
  public function getEntityId();
  public function getEntityIdId();
  public function setEntityIdId($entity_id);
  public function setEntityId(Entity $entity);

  // string | type  | Property type
  public function getType();
  public function setType($value);

  // string | value | Property value
  public function getValue();
  public function setValue($value);
}
