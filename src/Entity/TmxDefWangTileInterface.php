<?php

namespace Drupal\tmx\Entity;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Provides an interface for defining TMX Wang Tile Definition entities.
 *
 * @ingroup tmx
 */
interface TmxDefWangTileInterface extends ContentEntityInterface {

  // Add get/set methods for your configuration properties here.

  // int      | tmx_def_wang_set | Referenced Wang Set
  public function getDefWangSet();
  public function getDefWangSetId();  
  public function setDefWangSetId($entity_id);
  public function setDefWangSet(Entity $entity);

  // int      | tmx_def_tileitem |  Local ID of tile (Original: tileid)
  public function getDefTileitem();
  public function getDefTileitemId();
  public function setDefTileitemId($entity_id);
  public function setDefTileitem(Entity $entity);

  // bool     | dflip  |  Tile is flipped diagonally
  public function isDFlip();
  public function setDFlip($value);

  // bool     | hflip  |  Tile is flipped horizontally
  public function isHFlip();
  public function setHFlip($value);

  // bool     | vflip  |  Tile is flipped vertically
  public function isVFlip();
  public function setVFlip($value);

  // computed | wangcolors | Wang color indexes (uchar[8]) (array) (Original: wangid)
  public function getWangColors();
  public function setWangColors($collection);
  public function setWangColor($value);
}
