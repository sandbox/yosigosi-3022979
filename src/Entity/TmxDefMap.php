<?php

namespace Drupal\tmx\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\stdClass;
use Drupal\user\UserInterface;

/**
 * Defines the TMX Map Definition entity.
 *
 * @ingroup tmx
 *
 * @ContentEntityType(
 *   id = "tmx_def_map",
 *   label = @Translation("TMX Map Definition"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\tmx\TmxDefMapListBuilder",
 *     "views_data" = "Drupal\tmx\Entity\TmxDefMapViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\tmx\Form\TmxDefMapForm",
 *       "add" = "Drupal\tmx\Form\TmxDefMapForm",
 *       "edit" = "Drupal\tmx\Form\TmxDefMapForm",
 *       "delete" = "Drupal\tmx\Form\TmxDefMapDeleteForm",
 *     },
 *     "access" = "Drupal\tmx\TmxDefMapAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\tmx\TmxDefMapHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "tmx_def_map",
 *   admin_permission = "administer tmx",
 *   entity_keys = {
 *     "id" = "tmx_mid"
 *   },
 *   links = {
 *     "canonical" = "/admin/content/tmx/tmx_def_map/{tmx_def_map}",
 *     "add-form" = "/admin/content/tmx/tmx_def_map/add",
 *     "edit-form" = "/admin/content/tmx/tmx_def_map/{tmx_def_map}/edit",
 *     "delete-form" = "/admin/content/tmx/tmx_def_map/{tmx_def_map}/delete",
 *     "collection" = "/admin/content/tmx/def_maps",
 *   },
 *   field_ui_base_route = "tmx_def_map.settings"
 * )
 */
class TmxDefMap extends ContentEntityBase implements TmxDefMapInterface {

  /**
   * {@inheritdoc}
   * @see: core/lib/Drupal/Core/Field/Plugin/Field
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);
      
    // @see: core/lib/Drupal/Core/Field/Plugin/Field
      
    // type 	string 	map (since 1.0)
    $fields['type'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Type'))
      ->setDescription(t('Type (map) (since 1.0).'))
      ->setSettings([
          'max_length' => 50,
          'text_processing' => 0,
      ])
      ->setDisplayOptions('view', [
          'label' => 'above',
          'type' => 'string',
          'weight' => 1,
      ])
      ->setDisplayOptions('form', [
          'type' => 'string_textfield',
          'weight' => 1,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE)
      ->setDefaultValue('map');
  
    // orientation 	string 	orthogonal, isometric, staggered or hexagonal
    $fields['orientation'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Orientation'))
      ->setDescription(t('Orientation (orthogonal|isometric|staggered|hexagonal).'))
      ->setSettings([
          'max_length' => 50,
          'text_processing' => 0,
      ])
      ->setDisplayOptions('view', [
          'label' => 'above',
          'type' => 'string',
          'weight' => 2,
      ])
      ->setDisplayOptions('form', [
          'type' => 'string_textfield',
          'weight' => 2,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE)
      ->setDefaultValue('');
    
    
    // version 	number 	The JSON format version
    $fields['version'] = BaseFieldDefinition::create('float')
      ->setLabel(t('Version'))
      ->setDescription(t('The JSON format version.'))
      ->setSettings([
          'min' => 0,
      ])
      ->setDisplayOptions('view', [
          'label' => 'above',
          'type' => 'decimal',
          'weight' => 3,
      ])
      ->setDisplayOptions('form', [
          'type' => 'decimal',
          'weight' => 3,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE)
      ->setDefaultValue(0);
    
    // tiledversion 	string 	The Tiled version used to save the file
    $fields['tiledversion'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Tiled version'))
      ->setDescription(t('The Tiled version used to save the file(1.0|1.1|1.2).'))
      ->setSettings([
          'max_length' => 50,
          'text_processing' => 0,
      ])
      ->setDisplayOptions('view', [
          'label' => 'above',
          'type' => 'string',
          'weight' => 4,
      ])
      ->setDisplayOptions('form', [
          'type' => 'string_textfield',
          'weight' => 4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE)
      ->setDefaultValue('');
    
    // infinite 	bool 	Whether the map has infinite dimensions
    $fields['infinite'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Infinite dimensions'))
      ->setDescription(t('A boolean indicating whether the map has infinite dimensions.'))
      ->setDisplayOptions('view', [
          'label' => 'inline',
          'type' => 'boolean',
          'weight' => 5,
      ])
      ->setDisplayOptions('form', [
          'type' => 'boolean_checkbox',
          'settings' => [
              'display_label' => TRUE,
          ],
          'weight' => 5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE)
      ->setDefaultValue(FALSE);
      
    
    // height 	int 	Number of tile rows
    $fields['height'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Height (rows)'))
      ->setDescription(t('Number of tile rows.'))
      ->setSetting('size', 'normal')
      ->setSetting('unsigned', TRUE)
      ->setDisplayOptions('view', [
          'label' => 'inline',
          'type' => 'number_integer',
          'weight' => 6,
      ])
      ->setDisplayOptions('form', [
          'type' => 'number',
          'weight' => 6,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE)
      ->setDefaultValue(0);
    
    // width 	int 	Number of tile columns
    $fields['width'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Number of tile columns'))
      ->setDescription(t('Number of tile columns.'))
      ->setSetting('size', 'normal')
      ->setSetting('unsigned', TRUE)
      ->setDisplayOptions('view', [
          'label' => 'inline',
          'type' => 'number_integer',
          'weight' => 7,
      ])
      ->setDisplayOptions('form', [
          'type' => 'number',
          'weight' => 7,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE)
      ->setDefaultValue(0);
      
    // tileheight 	int 	Map grid height
    $fields['tileheight'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Map grid height'))
      ->setDescription(t('Map grid height.'))
      ->setSetting('size', 'normal')
      ->setSetting('unsigned', TRUE)
      ->setDisplayOptions('view', [
          'label' => 'inline',
          'type' => 'number_integer',
          'weight' => 8,
      ])
      ->setDisplayOptions('form', [
          'type' => 'number',
          'weight' => 8,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE)
      ->setDefaultValue(0);
    
    // tilewidth 	int 	Map grid width
    $fields['tilewidth'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Map grid width'))
      ->setDescription(t('Map grid width.'))
      ->setSetting('size', 'normal')
      ->setSetting('unsigned', TRUE)
      ->setDisplayOptions('view', [
          'label' => 'inline',
          'type' => 'number_integer',
          'weight' => 9,
      ])
      ->setDisplayOptions('form', [
          'type' => 'number',
          'weight' => 9,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE)
      ->setDefaultValue(0);
    
    // hexsidelength 	int 	Length of the side of a hex tile in pixels
    $fields['hexsidelength'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Length of the side of a hex tile'))
      ->setDescription(t('Length of the side of a hex tile in pixels.'))
      ->setSetting('size', 'normal')
      ->setSetting('unsigned', TRUE)
      ->setDisplayOptions('view', [
          'label' => 'inline',
          'type' => 'number_integer',
          'weight' => 10,
      ])
      ->setDisplayOptions('form', [
          'type' => 'number',
          'weight' => 10,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE)
      ->setDefaultValue(0);
    
    // backgroundcolor 	string 	Hex-formatted color (#RRGGBB or #AARRGGBB)
    // (optional)
    $fields['backgroundcolor'] = BaseFieldDefinition::create('string')
        ->setLabel(t('Background color'))
        ->setDescription(t('Hex-formatted color (#RRGGBB or #AARRGGBB) (optional).'))
        ->setSettings([
            'max_length' => 50,
            'text_processing' => 0,
        ])
        ->setDisplayOptions('view', [
            'label' => 'above',
            'type' => 'string',
            'weight' => 11,
        ])
        ->setDisplayOptions('form', [
            'type' => 'string_textfield',
            'weight' => 11,
        ])
        ->setDisplayConfigurable('form', TRUE)
        ->setDisplayConfigurable('view', TRUE)
        ->setRequired(TRUE)
        ->setDefaultValue('');
    
    // renderorder 	string 	Rendering direction
    // orthogonal maps only
    $fields['renderorder'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Rendering direction'))
      ->setDescription(t('Rendering direction (orthogonal maps only).'))
      ->setSettings([
          'max_length' => 50,
          'text_processing' => 0,
      ])
      ->setDisplayOptions('view', [
          'label' => 'above',
          'type' => 'string',
          'weight' => 12,
      ])
      ->setDisplayOptions('form', [
          'type' => 'string_textfield',
          'weight' => 12,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE)
      ->setDefaultValue('');
    
    
    // staggeraxis 	string 	x or y
    // staggered / hexagonal maps only
    $fields['staggeraxis'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Stagger Axis'))
      ->setDescription(t('x or y (staggered / hexagonal maps only).'))
      ->setSettings([
          'max_length' => 50,
          'text_processing' => 0,
      ])
      ->setDisplayOptions('view', [
          'label' => 'above',
          'type' => 'string',
          'weight' => 13,
      ])
      ->setDisplayOptions('form', [
          'type' => 'string_textfield',
          'weight' => 13,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE)
      ->setDefaultValue('');
    
    // staggerindex 	string 	odd or even
    // staggered / hexagonal maps only
    $fields['staggerindex'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Stagger Index'))
      ->setDescription(t('odd or even (staggered / hexagonal maps only).'))
      ->setSettings([
          'max_length' => 50,
          'text_processing' => 0,
      ])
      ->setDisplayOptions('view', [
          'label' => 'above',
          'type' => 'string',
          'weight' => 14,
      ])
      ->setDisplayOptions('form', [
          'type' => 'string_textfield',
          'weight' => 14,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE)
      ->setDefaultValue('');
    
    // nextlayerid 	int 	Auto-increments for each layer
    $fields['nextlayerid'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Auto-increments for each layer'))
      ->setDescription(t('Auto-increments for each layer.'))
      ->setSetting('size', 'normal')
      ->setSetting('unsigned', TRUE)
      ->setDisplayOptions('view', [
          'label' => 'inline',
          'type' => 'number_integer',
          'weight' => 15,
      ])
      ->setDisplayOptions('form', [
          'type' => 'number',
          'weight' => 15,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE)
      ->setDefaultValue(0);
    
    // nextobjectid 	int 	Auto-increments for each placed object
    $fields['nextobjectid'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Auto-increments for each placed object'))
      ->setDescription(t('Auto-increments for each placed object.'))
      ->setSetting('size', 'normal')
      ->setSetting('unsigned', TRUE)
      ->setDisplayOptions('view', [
          'label' => 'inline',
          'type' => 'number_integer',
          'weight' => 16,
      ])
      ->setDisplayOptions('form', [
          'type' => 'number',
          'weight' => 16,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE)
      ->setDefaultValue(0);
    
    return $fields;
  }

  // type 	string 	map (since 1.0)
  public function getType() {
    return $this->get('type')->value;
  }
  public function setType($value) {
    $this->set('type', $value);
    return $this;
  }
  
  // orientation 	string 	orthogonal, isometric, staggered or hexagonal
  public function getOrientation() {
    return $this->get('orientation')->value;
  }
  public function setOrientation($value) {
    $this->set('orientation', $value);
    return $this;
  }
  
  // version 	number 	The JSON format version
  public function getVersion() {
    return $this->get('version')->value;
  }
  public function setVersion($value) {
    $this->set('version', $value);
    return $this;
  }
  
  // tiledversion 	string 	The Tiled version used to save the file
  public function getTiledVersion() {
    return $this->get('tiledversion')->value;
  }
  public function setTiledVersion($value) {
    $this->set('tiledversion', $value);
    return $this;
  }
  
  // infinite 	bool 	Whether the map has infinite dimensions
  public function isInfinite() {
    return $this->get('infinite')->value;
  }
  public function setInfinite($value) {
    $this->set('infinite', $value);
    return $this;
  }
  
  // height 	int 	Number of tile rows
  public function geHeight() {
    return $this->get('height')->value;
  }
  public function setHeight($value) {
    $this->set('height', $value);
    return $this;
  }
  
  // width 	int 	Number of tile columns
  public function getWidth() {
    return $this->get('width')->value;
  }
  public function setWidth($value) {
    $this->set('width', $value);
    return $this;
  }
  
  // tileheight 	int 	Map grid height
  public function getTileHeight() {
    return $this->get('tileheight')->value;
  }
  public function setTileHeight($value) {
    $this->set('tileheight', $value);
    return $this;
  }
  
  // tilewidth 	int 	Map grid width
  public function getTileWidth() {
    return $this->get('tilewidth')->value;
  }
  public function setTileWidth($value) {
    $this->set('tilewidth', $value);
    return $this;
  }
  
  // hexsidelength 	int 	Length of the side of a hex tile in pixels
  public function getHexSideLength() {
    return $this->get('hexsidelength')->value;
  }
  public function setHexSideLength($value) {
    $this->set('hexsidelength', $value);
    return $this;
  }
  
  // backgroundcolor 	string 	Hex-formatted color (#RRGGBB or #AARRGGBB)
  // (optional)
  public function getBackgroundColor() {
    return $this->get('backgroundcolor')->value;
  }
  public function setBackgroundColor($value) {
    $this->set('backgroundcolor', $value);
    return $this;
  }
  
  // renderorder 	string 	Rendering direction
  // orthogonal maps only
  public function getRenderOrder() {
    return $this->get('renderorder')->value;
  }
  public function setRenderOrder($value) {
    $this->set('renderorder', $value);
    return $this;
  }
  
  // staggeraxis 	string 	x or y
  // staggered / hexagonal maps only
  public function getStaggerAxis() {
    return $this->get('staggeraxis')->value;
  }
  public function setStaggerAxis($value) {
    $this->set('staggeraxis', $value);
    return $this;
  }
  
  // staggerindex 	string 	odd or even
  // staggered / hexagonal maps only
  public function getStaggerIndex() {
    return $this->get('staggerindex')->value;
  }
  public function setStaggerIndex($value) {
    $this->set('staggerindex', $value);
    return $this;
  }
  
  // nextlayerid 	int 	Auto-increments for each layer
  public function getNextLayerId() {
    return $this->get('nextlayerid')->value;
  }
  public function setNextLayerId($value) {
    $this->set('nextlayerid', $value);
    return $this;
  }
  
  // nextobjectid 	int 	Auto-increments for each placed object
  public function getNextObjectId() {
    return $this->get('nextobjectid')->value;
  }
  public function setNextObjectId($value) {
    $this->set('nextobjectid', $value);
    return $this;
  }
  
  
  // properties 	Array of properties objects (name, value, type).
  public function getProperties() {
    return [];
  }
  
  // tilesets 	array 	Array of Tilesets ids
  public function getTilesets() {
    return [];
  }
  
  // layers 	array 	Array of Layers ids
  public function getLayers() {
    return [];
  }

}
