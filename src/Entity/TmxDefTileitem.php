<?php

namespace Drupal\tmx\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;

/**
 * Defines the TMX Tileitem Definition entity.
 *
 * @ingroup tmx
 *
 * @ContentEntityType(
 *   id = "tmx_def_tileitem",
 *   label = @Translation("TMX Tileitem Definition"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\tmx\TmxDefTileitemListBuilder",
 *     "views_data" = "Drupal\tmx\Entity\TmxDefTileitemViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\tmx\Form\TmxDefTileitemForm",
 *       "add" = "Drupal\tmx\Form\TmxDefTileitemForm",
 *       "edit" = "Drupal\tmx\Form\TmxDefTileitemForm",
 *       "delete" = "Drupal\tmx\Form\TmxDefTileitemDeleteForm",
 *     },
 *     "access" = "Drupal\tmx\TmxDefTileitemAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\tmx\TmxDefTileitemHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "tmx_def_tileitem",
 *   admin_permission = "administer tmx",
 *   entity_keys = {
 *     "id" = "tmx_tlid",
 *   },
 *   links = {
 *     "canonical" = "/admin/content/tmx/tmx_def_tileitem/{tmx_def_tileitem}",
 *     "add-form" = "/admin/content/tmx/tmx_def_tileitem/add",
 *     "edit-form" = "/admin/content/tmx/tmx_def_tileitem/{tmx_def_tileitem}/edit",
 *     "delete-form" = "/admin/content/tmx/tmx_def_tileitem/{tmx_def_tileitem}/delete",
 *     "collection" = "/admin/content/tmx/def_tileitems",
 *   },
 *   field_ui_base_route = "tmx_def_tileitem.settings"
 * )
 */
class TmxDefTileitem extends ContentEntityBase implements TmxDefTileitemInterface {

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    // int      | tmx_tlid |  TMX Tile Id

    // ref      | tmx_def_tileset | Referenced Tileset
    $fields['tmx_def_tileset'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Tileset Definition'))
      ->setDescription(t('Referenced Tileset.'))
      ->setSetting('target_type', 'tmx_def_tileset')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('view', [
          'label' => 'hidden',
          'type' => 'entity_reference_entity_id',
          'weight' => -1,
      ])
      ->setDisplayOptions('form', [
          'type' => 'entity_reference_autocomplete',
          'settings' => [
              'match_operator' => 'CONTAINS',
              'size' => '60',
              'autocomplete_type' => 'tags',
              'placeholder' => '',
          ],
          'weight' => -1,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE)
      ->setDefaultValue(0);

    // ref      | tmx_def_layer |  Layer with type objectgroup (optional) (Original: objectgroup)
    $fields['tmx_def_layer'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Layer Definition'))
      ->setDescription(t('Layer with type objectgroup (optional) (Original: objectgroup).'))
      ->setSetting('target_type', 'tmx_def_map')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('view', [
          'label' => 'hidden',
          'type' => 'entity_reference_entity_id',
          'weight' => -1,
      ])
      ->setDisplayOptions('form', [
          'type' => 'entity_reference_autocomplete',
          'settings' => [
              'match_operator' => 'CONTAINS',
              'size' => '60',
              'autocomplete_type' => 'tags',
              'placeholder' => '',
          ],
          'weight' => -1,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE)
      ->setDefaultValue(0);

    // int      | tid  |  Local ID of the tile (Original: id)
    $fields['tid'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Local ID'))
      ->setDescription(t('Local ID of the tile (Original: id).'))
      ->setSetting('size', 'normal')
      ->setSetting('unsigned', TRUE)
      ->setDisplayOptions('view', [
          'label' => 'inline',
          'type' => 'number_integer',
          'weight' => 5,
      ])
      ->setDisplayOptions('form', [
          'type' => 'number',
          'weight' => 5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE)
      ->setDefaultValue(0);

    // string   | type  |  The type of the tile (optional)
    $fields['type'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Type'))
      ->setDescription(t('The type of the tile (optional).'))
      ->setSettings([
          'max_length' => 50,
          'text_processing' => 0,
      ])
      ->setDisplayOptions('view', [
          'label' => 'above',
          'type' => 'string',
          'weight' => 3,
      ])
      ->setDisplayOptions('form', [
          'type' => 'string_textfield',
          'weight' => 3,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE)
      ->setDefaultValue('tileset');

    // string   | image  |  Image representing this tile (optional)
    $fields['image'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Image'))
      ->setDescription(t('Image used for tiles in this set.'))
      ->setSettings([
          'max_length' => 50,
          'text_processing' => 0,
      ])
      ->setDisplayOptions('view', [
          'label' => 'above',
          'type' => 'string',
          'weight' => 11,
      ])
      ->setDisplayOptions('form', [
          'type' => 'string_textfield',
          'weight' => 11,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(FALSE)
      ->setDefaultValue('');

    // ref       | tmx_img_fid |  Image used by this layer. imagelayer only.
    $fields['tmx_img_fid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Image file id'))
      ->setDescription(t('The Image file id that implements the field image.'))
      ->setSetting('target_type', 'file')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('view', [
          'label' => 'hidden',
          'type' => 'entity_reference_label',
          'weight' => 12,
      ])
      ->setDisplayOptions('form', [
          'type' => 'entity_reference_autocomplete',
          'settings' => [
              'match_operator' => 'CONTAINS',
              'size' => '60',
              'autocomplete_type' => 'tags',
              'placeholder' => '',
          ],
          'weight' => 12,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE)
      ->setDefaultValue(0);

    // int      | imagewidth  |  Width of the tile image in pixels
    $fields['imagewidth'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Image width'))
      ->setDescription(t('Width of source image in pixels.'))
      ->setSetting('size', 'normal')
      ->setSetting('unsigned', TRUE)
      ->setDisplayOptions('view', [
          'label' => 'inline',
          'type' => 'number_integer',
          'weight' => 5,
      ])
      ->setDisplayOptions('form', [
          'type' => 'number',
          'weight' => 5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE)
      ->setDefaultValue(0);

    // int      | imageheight  |  Height of the tile image in pixels
    $fields['imageheight'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Image height'))
      ->setDescription(t('Height of source image in pixels.'))
      ->setSetting('size', 'normal')
      ->setSetting('unsigned', TRUE)
      ->setDisplayOptions('view', [
          'label' => 'inline',
          'type' => 'number_integer',
          'weight' => 5,
      ])
      ->setDisplayOptions('form', [
          'type' => 'number',
          'weight' => 5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE)
      ->setDefaultValue(0);

    return $fields;
  }

  // ref      | tmx_def_tileset | Referenced Tileset
  public function getDefTileset() {
    return $this->get('tmx_def_tileset')->entity;
  }
  public function getDefTilesetId() {
    return $this->get('tmx_def_tileset')->target_id;
  }
  public function setDefTilesetId($entity_id) {
    $this->set('tmx_def_tileset', $entity_id);
    return $this;
  }
  public function setDefTileset(Entity $entity) {
    $this->set('tmx_def_tileset', $entity->id());
    return $this;
  }

  // ref      | tmx_def_layer |  Layer with type objectgroup (optional) (Original: objectgroup)
  public function getDefLayer() {
    return $this->get('tmx_def_layer')->entity;
  }
  public function getDefLayerId() {
    return $this->get('tmx_def_layer')->target_id;
  }
  public function setDefLayerId($entity_id) {
    $this->set('tmx_def_layer', $entity_id);
    return $this;
  }
  public function setDefLayer(Entity $entity) {
    $this->set('tmx_def_layer', $entity->id());
    return $this;
  }

  // int      | tid  |  Local ID of the tile (Original: id)
  public function getTid() {
    return $this->get('tid')->value;
  }
  public function setTid($value) {
    $this->set('tid', $value);
    return $this;
  }

  // string   | type  |  The type of the tile (optional)
  public function getType() {
    return $this->get('type')->value;
  }
  public function setType($value) {
    $this->set('type', $value);
    return $this;
  }

  // string   | image  |  Image representing this tile (optional)
  public function getImage() {
    return $this->get('image')->value;
  }
  public function setImage($value) {
    $this->set('image', $value);
    return $this;
  }

  // ref       | tmx_img_fid |  Image used by this layer. imagelayer only.
  public function getImageFile() {
    return $this->get('tmx_img_fid')->entity;
  }
  public function getImageFileId() {
    return $this->get('tmx_img_fid')->target_id;
  }
  public function setImageFileId($entity_id) {
    $this->set('tmx_img_fid', $entity_id);
    return $this;
  }
  public function setImageFile(Entity $entity) {
    $this->set('tmx_img_fid', $entity->id());
    return $this;
  }

  // int      | imagewidth  |  Width of the tile image in pixels
  public function getImageWidth() {
    return $this->get('imagewidth')->value;
  }
  public function setImageWidth($value) {
    $this->set('imagewidth', $value);
    return $this;
  }

  // int      | imageheight  |  Height of the tile image in pixels
  public function getImageHeight() {
    return $this->get('imageheight')->value;
  }
  public function setImageHeight($value) {
    $this->set('imageheight', $value);
    return $this;
  }


  // computed | properties |  A list of properties (name, value, type) (array)
  public function getProperties() {
    return []; // TBD
  }
  public function setProperties($collection) {
    // TBD
  }
  public function setProperty($value) {
    // TBD
  }

  // computed | animation  |  Array of Frames (array)
  public function getFrames() {
    return []; // TBD
  }
  public function setFrames($collection) {
    // TBD
  }
  public function setFrame($value) {
    // TBD
  }

  // computed | terrain    |  Index of terrain for each corner of tile (array)
  public function getTerrains() {
    return []; // TBD
  }
  public function setTerrains($collection) {
    // TBD
  }
  public function setTerrain($value) {
    // TBD
  }

}
