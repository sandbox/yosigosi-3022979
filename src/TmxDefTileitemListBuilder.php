<?php

namespace Drupal\tmx;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of TMX Tileitem Definition entities.
 *
 * @ingroup tmx
 */
class TmxDefTileitemListBuilder extends EntityListBuilder {


  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('TMX Tileitem Definition ID');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\tmx\Entity\TmxDefTileitem */
    $row['id'] = $entity->id();
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.tmx_def_tileitem.edit_form',
      ['tmx_def_tileitem' => $entity->id()]
    );
    return $row + parent::buildRow($entity);
  }

}
