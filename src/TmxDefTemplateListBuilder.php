<?php

namespace Drupal\tmx;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of TMX Template Definition entities.
 *
 * @ingroup tmx
 */
class TmxDefTemplateListBuilder extends EntityListBuilder {


  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('TMX Template Definition ID');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\tmx\Entity\TmxDefTemplate */
    $row['id'] = $entity->id();
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.tmx_def_template.edit_form',
      ['tmx_def_template' => $entity->id()]
    );
    return $row + parent::buildRow($entity);
  }

}
