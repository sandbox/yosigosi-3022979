<?php

namespace Drupal\tmx;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the TMX Wang Color Definition entity.
 *
 * @see \Drupal\tmx\Entity\TmxDefWangColor.
 */
class TmxDefWangColorAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\tmx\Entity\TmxDefWangColorInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished tmx wang color definition entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published tmx wang color definition entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit tmx wang color definition entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete tmx wang color definition entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add tmx wang color definition entities');
  }

}
