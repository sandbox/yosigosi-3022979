<?php

namespace Drupal\tmx;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of TMX Frame Definition entities.
 *
 * @ingroup tmx
 */
class TmxDefFrameListBuilder extends EntityListBuilder {


  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('TMX Frame Definition ID');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\tmx\Entity\TmxDefFrame */
    $row['id'] = $entity->id();
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.tmx_def_frame.edit_form',
      ['tmx_def_frame' => $entity->id()]
    );
    return $row + parent::buildRow($entity);
  }

}
