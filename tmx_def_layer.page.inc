<?php

/**
 * @file
 * Contains tmx_def_layer.page.inc.
 *
 * Page callback for TMX Layer Definition entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for TMX Layer Definition templates.
 *
 * Default template: tmx_def_layer.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_tmx_def_layer(array &$variables) {
  // Fetch TmxDefLayer Entity Object.
  $tmx_def_layer = $variables['elements']['#tmx_def_layer'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
